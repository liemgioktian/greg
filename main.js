jQuery(function () {
  var controller  = jQuery('body').attr('data-controller')
  var site_url    = jQuery('body').attr('data-site-url')
  var dtopt       = { 
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax": {
        "url": jQuery('.main-table').attr('data-source'),
        "type": "POST"
    }
  }

  formUtils()
  if (jQuery('.main-table').length > 0) jQuery('.main-table').each(function () {
    var isDoc = $(this).is('.document-table')
    if (isDoc) {
      dtopt.order = [[ 7, 'desc' ]]
      dtopt.drawCallback = function( settings ) {
        $('a[data-action="edit-document"]').click(function () {
          var source = $(this).attr('data-source')
          $('#editDoc .modal-body').html('<div class="loader"></div>')
          $('#editDoc').modal('show')
          $.get(source, function (html) {
            $('#editDoc .modal-body').append('<div class="hidden temporary"></div>')
            $('.temporary').html(html)
            var form = $('#editDoc .modal-body form').clone()
            $('#editDoc .modal-body').html(form)
            $('.temporary').remove()
            editDocAfterRender()
            formUtils()
          })
        })
      }
      dtopt.initComplete = function () {
        var r = $('.main-table tfoot tr');
        r.find('th').each(function () {
          $(this).css('padding', 8);
        });
        $('.main-table thead').append(r);
        $('#search_0').css('text-align', 'center');
      }
    }
    var dtobj = jQuery(this).DataTable(dtopt)
    if (isDoc) {
      dtobj.columns().every(function () {
        var that = this
        $('input, select', this.footer()).on('keyup change', function () {
          if (that.search() !== this.value ) that.search(this.value).draw()
        })
      })
      $('.delete-multiple').click(function () {
        var ids = []
        $('[name="doc-check"]').each(function () {
          if ($(this).is(':checked')) ids.push($(this).val())
        })
        if (ids.length > 0) window.location = $(this).attr('data-href') +'?ids='+ ids.join(',')
      })
    }
  })

  if (jQuery('form.form-upload-loader').length > 0) {
    var form = jQuery('form.form-upload-loader')
    var div  = form.find('div')
    var input= form.find('input')
    div.click(function () {
      input.click()
    })
    input.change(function () {
      form.submit()
    })
  }

  if ($('a[href*="cron"]').length > 0) $('a[href*="cron"]').attr('target', '_blank')
  if ($('[name="message"]').length > 0) $('[name="message"]').summernote({
    callbacks: {
      onInit: function() {
        $('[name="files"]').remove()
      }
    }
  })

  function formUtils () {
    if (jQuery('form[data-validation-url]').length > 0) {
      var form = jQuery('form[data-validation-url]')
      form.unbind('submit').bind('submit', function (e) {
        e.preventDefault()
        if (jQuery('[data-currency]').length > 0) jQuery('[data-currency]').blur()
        jQuery.post(form.attr('data-validation-url'), form.serialize(), function (valid) {
          if ('valid' !== valid) {
            jQuery('span.error-message').html(valid)
            jQuery('div.validation-error').removeClass('hidden')
          } else {
            if (jQuery('[name="rab"]').length > 0) jQuery('[name="rab"]').val(jQuery('[name="rab"]').val().split('.').join(''))
            form.unbind('submit').submit()
          }
        })
      })
    }
    if (jQuery('.date').length > 0) jQuery('.date').datetimepicker({format: 'DD-MM-YYYY'})
    if (jQuery('select').length > 0) jQuery('select').select2()
    if (jQuery('[data-currency]').length > 0)
      jQuery('[data-currency]').maskMoney({
        prefix: 'Rp ',
        thousands: '.',
        decimal: ',',
        precision: 0,
        affixesStay: false
      }).maskMoney('mask')
  }

  function editDocAfterRender () {
    $('button[type="submit"]').html('Verifikasi')
    if ($('input[type="submit"]').length > 0) {
      $('button[type="submit"]').hide()
      $('input[type="submit"]').parent().parent().detach().insertAfter('[name="id"]')

      var ajenis = ['Pengadaan Langsung', 'Penunjukan Langsung', 'Lelang Terbatas', 'Lelang Terbuka', 'Lelang Pascakualifikasi', 'Lelang Prakualifikasi']
      var ojenis = {}
      var isLelangPage = true
      for (var j in ajenis) {
        ojenis[ajenis[j]] = $('[value="'+ajenis[j]+'"]')
        isLelangPage *= ojenis[ajenis[j]].length > 0
        ojenis[ajenis[j]] = ojenis[ajenis[j]].parent().parent()
      }
      if (isLelangPage) {
        ojenis['Lelang Pascakualifikasi'].hide()
        ojenis['Lelang Prakualifikasi'].hide()
        ojenis['Lelang Terbuka'].click(showLelang)
        ojenis['Lelang Terbatas'].click(showLelang)
        function showLelang (e) {
          e.preventDefault()
          $('input[type="submit"]').parent().parent().hide()
          ojenis['Lelang Pascakualifikasi'].show()
          ojenis['Lelang Prakualifikasi'].show()
        }
      }

      var aBA = ['Next', 'Pemeriksaan Sesuai', 'Pemeriksaan Tidak Sesuai']
      var oBA = {}
      var isBApage = true
      for (var a in aBA) {
        oBA[aBA[a]] = $('[value="'+aBA[a]+'"]')
        isBApage *= oBA[aBA[a]].length > 0
        oBA[aBA[a]] = oBA[aBA[a]].parent().parent()
      }
      if (isBApage) {
        oBA['Next'].hide()
        oBA['Pemeriksaan Sesuai'].click(function (e) {
          e.preventDefault()
          $('input[type="submit"]').parent().parent().hide()
          oBA['Next'].show()
        })
      }

      if (10 < $('input[type="submit"]').length) $('button[type="submit"]').html('Update').show()
    }
  }
})