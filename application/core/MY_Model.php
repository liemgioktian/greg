<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

  function __construct () {
    $this->load->database();
  }

  var $thead = array();

  function validate ($record) {
    return 'valid';
  }

  function save ($record) {
    return isset ($record['id']) ? $this->update($record) : $this->create($record);
  }

  function create ($record) {
    $this->db->insert($this->table, $record);
    return $this->db->insert_id ();
  }

  function update ($record) {
    $this->db->where('id', $record['id'])->update($this->table, $record);
    return $record['id'];
  }

  function findOne ($param) {
    if (!is_array($param)) $param = array('id' => $param);
    return $this->db->get_where($this->table, $param)->row_array();
  }

  function find ($param = array()) {
    return $this->db->get_where($this->table, $param)->result();
  }

  function delete ($id) {
    return $this->db->where('id', $id)->delete($this->table);
  }

  function getThead () {
    return $this->thead;
  }

  function getForm ($id = false) {
    $form = $id ? $this->prepopulate($id) : $this->form;

    if ($id) $form[] = array(
      'name' => 'id',
      'type' => 'hidden',
      'value'=> $id
    );

    foreach ($form as &$f) {
      if (isset ($f['options'])) $f['type'] = 'select';
      if (!isset ($f['type'])) $f['type']   = 'text';

      if (!isset ($f['value'])) $f['value']       = '';

      if (!isset ($f['required'])) $f['required'] = '';
      else $f['required'] = 'required="required"';

      $f['disabled'] = !isset($f['disabled']) ? '' : 'disabled="disabled"';
    }
    return $form;
  }

  function prepopulate ($id) {
    $record = $this->findOne($id);
    foreach ($this->form as &$f) {
      if (strpos($f['name'], '[]')) {
        $singular  = str_replace('[]', '', $f['name']);
        $f['value']= explode(',', $record[$singular]);
      }
      else $f['value'] = $record[$f['name']];
    }
    return $this->form;
  }

  function datatables ($controller){
    $this->load->library('datatables');
    foreach ($this->thead as $th) $this->datatables->select($th['field']);
    $this->datatables->select('id');
    $this->datatables->edit_column('id', 
      '<a class="btn btn-xs btn-warning" href="'. site_url($controller . '/read/').'$1"><i class="fa fa-edit"></i></a>'.
      '<a class="btn btn-xs btn-danger" href="'. site_url($controller . '/delete/').'$1"><i class="fa fa-trash"></i></a>',
    'id');
    $this->datatables->from($this->table);
    return $this->datatables->generate();
  }
}