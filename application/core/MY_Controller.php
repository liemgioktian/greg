<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  var $loggedin     = false;
  var $page_title   = '';
  var $page_caption = '';
  var $table_title  = '';
  var $table_caption= '';
  var $controller   = '';
  var $model        = '';

  function __construct () {
    parent::__construct();
    $data = array ();
    $this->load->helper('url');
    $this->loggedin = $this->session->userdata('username');

    if (!$this->loggedin && $this->input->post()) {
      $this->load->model('Adminmodel');
      $this->loggedin = $this->Adminmodel->login($this->input->post());
      if (!$this->loggedin) $data = array ('loginfail' => true);
    }

    if (!$this->loggedin) {
      $controller   = 'Document';
      $model        = 'Documentmodel';
      $table_title  = 'Pekerjaan';
      $table_caption= 'Daftar Semua Pekerjaan';

      $this->load->model($model);

      $data['pagetype']     = 'table-document';
      $data['dataSource']   = site_url('document/opendata');
      $data['controller']   = $controller;
      $data['page_title']   = '';
      $data['page_caption'] = '';
      $data['table_title']  = $table_title;
      $data['table_caption']= $table_caption;
      $data['thead']        = $this->$model->getThead();
      $data['tbody']        = array();

      $this->load->model('Docstatmodel');
      $data['almostLate'] = $this->Docstatmodel->almostLate();

      $data['menu'] = array();
      $this->load->view('login', $data);
    }
  }

  public function loadview ($view, $data = array())
  {
    if (!$this->loggedin) return false;
    $data['menu'] = $this->menu();
    $this->load->model('Docstatmodel');
    $data['almostLate'] = $this->Docstatmodel->almostLate();
    if ('table-document' === $data['pagetype']) {
      if ('SUPERADMIN' === $this->session->userdata('role_name')) $data['allowAddDoc'] = true;
      else {
        $this->load->model('Rolestatusmodel');
        if (in_array(0, $this->Rolestatusmodel->getStatus())) $data['allowAddDoc'] = true;
      }
    }
    $this->load->view($view, $data);
  }

  private function menu () {
    $menu = array();
    if ('SUPERADMIN' === $this->session->userdata('role_name')) {

      $menu[]   = array (
        'url'   => site_url('admin/index'),
        'title' => 'Admin',
        'icon'  => 'child',
        'description' => "Anda dapat mengatur login admin melalui menu ini, baik menambah admin baru, 
          mengubah bidang admin, mengganti password, atau menghapus login administrator"
      );

      $menu[]   = array (
        'url'   => site_url('document/index'),
        'title' => 'Pekerjaan',
        'icon'  => 'files-o',
        'description' => "Halaman ini memungkinkan anda menemukan dan mengakses semua pekerjaan yang ada dalam aplikasi, 
          memasukkan pekerjaan baru, edit data, maupun menghapusnya"
      );

      $menu[]   = array (
        'url'   => site_url('status/index'),
        'title' => 'Proses Pekerjaan',
        'icon'  => 'tags',
        'description' => "Untuk memantau progress setiap pekerjaan, terlebih dahulu anda harus mendefinisikan proses apa saja yang
          dapat melekat pada pekerjaan-pekerjaan tersebut. 
          <br/> Proses awal berarti proses yang akan diberikan pada pekerjaan saat pertama kali diinput
          <br/> Menghapus proses pekerjaan akan berakibat pada pekerjaan yang berada dalam proses tersebut akan kehilangan status"
      );

      $menu[]   = array (
        'url'   => site_url('role/index'),
        'title' => 'Bidang Admin',
        'icon'  => 'sitemap',
        'description' => "Membagi administrator dalam bidang-bidang kerja masing-masing, khusus bidang superadmin harus tetap ada, untuk memanage aplikasi"
      );

      $menu[]   = array (
        'url'   => site_url('rolestatus/index'),
        'title' => 'Authentikasi Proses Pekerjaan',
        'icon'  => 'check-square-o',
        'description' => "Menu ini memasangkan antara bidang administrator dengan proses pekerjaan, sehingga ketika seorang administrator mengakses aplikasi
          maka secara otomatis disajikan pekerjaan yang prosesnya sesuai dengan bidangnya"
      );

      $menu[]   = array (
        'url'   => site_url('fieldstatus/index'),
        'title' => 'Field Penanda Proses Pekerjaan',
        'icon'  => 'key',
        'description' => "Proses suatu pekerjaan akan berubah mengikuti perubahan data pekerjaan tersebut,
          disinilah anda mengatur, data apa yang menyebabkan proses pekerjaan berubah,
          misal: anda set data 'Nomor TOR' untuk proses 'TOR', maka ketika kolom TOR diisi dan disubmit, maka secara otomatis proses pekerjaan akan berubah menjadi TOR"
      );

      $menu[]   = array (
        'url'   => site_url('fieldappearance/index'),
        'title' => 'Field Tampil Sesuai Proses Pekerjaan',
        'icon'  => 'eye-slash',
        'description' => "Menu ini digunakan untuk mengatur kemunculan suatu field pekerjaan pada status pekerjaan tertentu, misal :
        field No. TOR hanya akan ditampilkan saat pekerjaan berstatus WAITING TOR "
      );

      $menu[]   = array (
        'url'   => site_url('holiday/index'),
        'title' => 'Hari Libur',
        'icon'  => 'calendar',
        'description' => "Pengaturan hari libur diluar weekend"
      );

      $menu[]   = array (
        'url'   => site_url('dashboard/userguide'),
        'title' => 'User Guide',
        'icon'  => 'book'
      );

    } else {
      $menu[]   = array (
        'url'   => site_url('document/index'),
        'title' => 'Semua Pekerjaan',
        'icon'  => 'files-o'
      );
      $menu[]   = array (
        'url'   => site_url('document/mine'),
        'title' => 'Pekerjaan ' . ucfirst(strtolower($this->session->userdata('role_name'))),
        'icon'  => 'file'
      );
    }
    return $menu;
  }

  public function json (){
    if ($this->session->userdata('id') && strlen($this->model) > 0) {
      $model = $this->model;
      $this->load->model($model);
      echo $this->$model->datatables($this->controller);
    }
  }

  public function index()
  {
    $data                 = array();
    $data['pagetype']     = 'table';
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $data['thead']        = array();
    $data['tbody']        = array();
    if (strlen($this->model) > 0) {
      $model = $this->model;
      $this->load->model($model);
      if ($this->input->post() && !$this->input->post('login')) {
        if ($this->input->post('delete_id')) $this->$model->delete($this->input->post('delete_id'));
        else $this->$model->save($this->input->post());
      }
      $data['thead'] = $this->$model->getThead();
      // $data['tbody'] = $this->$model->find();
    }
    $this->loadview('main', $data);
  }

  function read ($id) {
    $data                 = array();
    $data['pagetype']     = 'form';
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $model                = $this->model;
    $this->load->model($model);
    $data['form']         = $this->$model->getForm($id);
    $this->loadview('main', $data);
  }

  function create () {
    $data                 = array();
    $data['pagetype']     = 'form';
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $model                = $this->model;
    $this->load->model($model);
    $data['form']         = $this->$model->getForm();
    $this->loadview('main', $data);
  }

  function validate () {
    $model = $this->model;
    $this->load->model($model);
    echo $this->$model->validate($this->input->post());
  }

  function delete ($id) {
    $data                 = array();
    $data['pagetype']     = 'confirm';
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $data['delete_id']    = $id;
    $this->loadview('main', $data);
  }

}