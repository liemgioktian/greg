<!-- Datatables -->
<link href="<?= base_url () ?>vendors/datatables.net/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url () ?>vendors/datatables.net/css/scroller.bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url () ?>vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<style type="text/css">
.loader {
  margin-left: auto;
  margin-right: auto;
  border: 16px solid #f3f3f3;
  border-top: 16px solid #3498db;
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?= $page_title ?> <small><?= $page_caption ?></small></h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?= $table_title ?><small><?= '| ' . $table_caption ?></small></h2>
            <?php if ($this->session->userdata('username')): ?>
            <div class="btn-group pull-right col-sm-7">

              <a class="btn btn-warning btn-sm col-xs-12 col-sm-2 pull-right" href="<?= site_url($controller . '/export') ?>">
                <i class="fa fa-file-excel-o"></i> Export
              </a>

              <?php if (isset ($allowAddDoc)) : ?>
              <a class="btn btn-primary btn-sm col-xs-12 col-sm-2" href="<?= site_url($controller . '/create') ?>">
                <i class="fa fa-plus"></i> Add New
              </a>
              <a class="btn btn-info btn-sm col-xs-12 col-sm-4" href="<?= site_url($controller . '/download') ?>">
                <i class="fa fa-download"></i> Download Template
              </a>
              <form class="btn btn-success btn-sm form-upload-loader col-xs-12 col-sm-2" 
              enctype="multipart/form-data"
              method="POST" action="<?= site_url('Document/upload') ?>">
                <div><i class="fa fa-upload"></i> Import</div>
                <input type="file" name="loader" class="hidden" />
              </form>
              <a class="btn btn-danger btn-sm col-xs-12 col-sm-2 delete-multiple" data-href="<?= site_url('document/delete_multiple') ?>">
                <i class="fa fa-trash"></i> Delete
              </a>
              <?php endif ?>

            </div>
            <?php endif ?>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-striped table-bordered main-table document-table" data-source="<?= $dataSource ?>">

              <thead>
                <tr>
                  <?php foreach ($thead as $th) : ?>
                  <th><?= $th['label'] ?></th>
                  <?php endforeach ?>
                  <th></th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                  <th>
                    <input type="hidden">
                  </th>
                  <th><input type="text" class="form-control"></th>
                  <th>
                    <select class="form-control">
                      <option value="">All</option>
                      <option value="AO PJB - Jasa OM">AO PJB - Jasa OM</option>
                      <option value="AO PJB - K3">AO PJB - K3</option>
                      <option value="AO PJB - Kimia">AO PJB - Kimia</option>
                      <option value="AO PJB - Lingkungan">AO PJB - Lingkungan</option>
                      <option value="AO PJB - Preventive Maintenance">AO PJB - Preventive Maintenance</option>
                      <option value="AO PJB - Predictive Maintenance">AO PJB - Predictive Maintenance</option>
                      <option value="AO PJB - Corrective Maintenance">AO PJB - Corrective Maintenance</option>
                      <option value="AO PJB - Overhoul/Inspection">AO PJB - Overhoul/Inspection</option>
                      <option value="AO PJB - Engineering/Project/Modifikasi">AO PJB - Engineering/Project/Modifikasi</option>
                      <option value="AO PJB - Non Instalasi">AO PJB - Non Instalasi</option>
                      <option value="AI PJB">AI PJB</option>
                      <option value="AI PLN">AI PLN</option>
                    </select>
                  </th>
                  <th><input type="text" class="form-control"></th>
                  <th><input type="text" class="form-control"></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>

              <tbody>
                <?php foreach ($tbody as $tb) : ?>
                <tr>
                  <?php foreach ($thead as $th) : ?>
                    <td><?= $tb->$th['field'] ?></td>
                  <?php endforeach ?>
                  <td class="text-center">
                    <a class="btn btn-xs btn-warning" href="<?= site_url("$controller/read/$tb->id") ?>"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-xs btn-danger" href="<?= site_url("$controller/delete/$tb->id") ?>"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php endforeach ?>
              </tbody>

            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="editDoc">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

<!-- Datatables -->
<script src="<?= base_url () ?>vendors/select2/dist/js/select2.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/dataTables.scroller.min.js"></script>