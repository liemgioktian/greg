<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3><?= $this->session->userdata('role_name') ?></h3>
    <ul class="nav side-menu">
      <?php foreach ($menu as $m): ?>
      <li>
        <a href="<?= $m['url'] ?>">
          <i class="fa fa-<?= $m['icon'] ?>"></i> <?= $m['title'] ?>
        </a>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>
</div>