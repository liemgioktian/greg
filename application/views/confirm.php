<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

      <div class="title_left">
        <h3><?= $page_title ?></h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        </div>
      </div>

    </div>
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?= $table_title ?> <small><?= $table_caption ?></small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <div class="col-xs-12 text-center">
              <h2>Are you sure ?</h2>
            </div>
            <div class="col-xs-12 text-center">
              <form method="POST" action="<?= site_url($controller) ?>">
                <input type="hidden" name="delete_id" value="<?= $delete_id ?>" />
                <a href="<?= site_url($controller) ?>" class="btn btn-sm btn-warning">No</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Yes" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>