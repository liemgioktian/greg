<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?= $page_title ?> <small><?= $page_caption ?></small></h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?= $table_title ?><small><?= '| ' . $table_caption ?></small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <ul class="quick-list" style="width:100%">
            <?php foreach ($menu as $m): if ($m['title'] !== 'User Guide'): ?>
              <li style="white-space:normal">
                <i class="fa fa-<?= $m['icon'] ?>"></i>
                <b><?= $m['title'] ?></b>
                <p><?= $m['description'] ?></p>
              </li>
            <?php endif; endforeach; ?>
            </ul>

            <ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>