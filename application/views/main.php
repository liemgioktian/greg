<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="manifest.json" rel="manifest">

    <title>PT. PJB | Procurement</title>
    <link href="<?= base_url () ?>favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link href="<?= base_url () ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url () ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url () ?>vendors/summernote/summernote.css" rel="stylesheet">
    <link href="<?= base_url () ?>build/css/custom.min.css" rel="stylesheet">

    <script src="<?= base_url () ?>vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url () ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url () ?>vendors/summernote/summernote.min.js"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;background: white">
              <a href="<?= base_url() ?>">
                <img src="<?= base_url('pjb-xs.png') ?>" class="hidden-lg" id="logo_small">
                <img src="<?= base_url('pjb-lg.png') ?>" class="hidden-sm hidden-xs" id="logo_big">
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?= base_url () ?>images/electrician.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?= $this->session->userdata('username') ?></h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php include_once(APPPATH . 'views/menu.php') ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="index.php/logout">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?= base_url () ?>images/electrician.png" alt=""><?= $this->session->userdata('username') ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?= site_url('logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-calendar"></i>
                    <span class="badge bg-green"><?= count($almostLate) ?></span>
                  </a>
                  <ul id="" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <?php foreach ($almostLate as $doc): ?>
                    <li>
                      <a>
                        <span>
                          <span><?= $doc->docname ?></span>
                        </span>
                        <span class="message">
                          <b><?= "$doc->statname $doc->deadline" ?></b>
                        </span>
                      </a>
                    </li>
                  <?php endforeach ?>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <?php require_once(APPPATH . "views/{$pagetype}.php") ?>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <script src="<?= base_url () ?>/build/js/custom.js"></script>
    <script src="<?= base_url () ?>main.js"></script>
  </body>
</html>
