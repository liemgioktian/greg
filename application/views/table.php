<!-- Datatables -->
<link href="<?= base_url () ?>vendors/datatables.net/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url () ?>vendors/datatables.net/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3><?= $page_title ?> <small><?= $page_caption ?></small></h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?= $table_title ?><small><?= '| ' . $table_caption ?></small></h2>
            <a class="btn pull-right btn-primary btn-sm" href="<?= site_url($controller . '/create') ?>">
              <i class="fa fa-plus"></i> Add New
            </a>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-striped table-bordered main-table" data-source="<?= site_url($controller . '/json') ?>">

              <thead>
                <tr>
                  <?php foreach ($thead as $th) : ?>
                  <th><?= $th['label'] ?></th>
                  <?php endforeach ?>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <?php foreach ($tbody as $tb) : ?>
                <tr>
                  <?php foreach ($thead as $th) : ?>
                    <td><?= $tb->$th['field'] ?></td>
                  <?php endforeach ?>
                  <td class="text-center">
                    <a class="btn btn-xs btn-warning" href="<?= site_url("$controller/read/$tb->id") ?>"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-xs btn-danger" href="<?= site_url("$controller/delete/$tb->id") ?>"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php endforeach ?>
              </tbody>

            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- Datatables -->
<script src="<?= base_url () ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url () ?>vendors/datatables.net/js/dataTables.scroller.min.js"></script>