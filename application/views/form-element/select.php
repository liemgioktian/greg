<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12"><?= $field['label'] ?></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select class="form-control" name="<?= $field['name'] ?>" <?= $field['disabled'] ?> <?= isset($field['multiple']) ? 'multiple="multiple"': '' ?>>
      <?php foreach ($field['options'] as $opt): ?>
      <option <?= $opt['value'] === $field['value'] || (is_array($field['value']) && in_array($opt['value'], $field['value'])) ? 'selected="selected"':'' ?> value="<?= $opt['value'] ?>"><?= $opt['text'] ?></option>
      <?php endforeach ?>
    </select>
  </div>
</div>