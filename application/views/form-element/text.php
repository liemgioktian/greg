<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12"
  for="<?= $field['name'] ?>"><?= $field['label'] ?>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" <?= $field['required'] ?> <?= $field['disabled'] ?> value="<?= $field['value'] ?>"  
    name="<?= $field['name'] ?>" class="form-control col-md-7 col-xs-12">
    <?php if (isset ($field['suffix'])): ?><small class="form-text text-muted"><?= $field['suffix'] ?></small><?php endif ?>
  </div>
</div>