<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fieldstatus extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Fieldstatus';
    $this->model        = 'Fieldstatusmodel';
    $this->table_title  = 'Field Penanda Proses Pekerjaan';
    $this->table_caption= 'Pengaturan field penanda proses pekerjaan';
  }

}