<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fieldappearance extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Fieldappearance';
    $this->model        = 'Fieldappearancemodel';
    $this->table_title  = 'Field tampil sesuai proses pekerjaan';
    $this->table_caption= 'Pengaturan tampilan field pekerjaan sesuai proses pekerjaan';
  }

}