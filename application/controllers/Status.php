<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Status';
    $this->model        = 'Statusmodel';
    $this->table_title  = 'Proses Pekerjaan';
    $this->table_caption= 'Pengaturan proses pekerjaan';
  }

  function set_as_initial ($id) {
    $model = $this->model;
    $this->load->model($model);
    $record = $this->$model->findOne($id);
    $record['is_initial'] = '1';
    $this->$model->save($record);
    redirect(site_url($this->controller));
  }

  function set_as_final ($id) {
    $model = $this->model;
    $this->load->model($model);
    $record = $this->$model->findOne($id);
    $record['is_final'] = '1';
    $this->$model->save($record);
    redirect(site_url($this->controller));
  }

  function cron () {
    $this->load->model('Statusmodel');
    $this->Statusmodel->cron();
    return true;
  }

}