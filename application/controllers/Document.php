<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Document';
    $this->model        = 'Documentmodel';
    $this->table_title  = 'Pekerjaan';
    $this->table_caption= 'Daftar Semua Pekerjaan';
  }

  public function index()
  {
    $data                 = array();
    $data['pagetype']     = 'table-document';
    $data['dataSource']   = site_url('document/json');
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $data['thead']        = array();
    $data['tbody']        = array();
    if (strlen($this->model) > 0) {
      $model = $this->model;
      $this->load->model($model);
      if ($this->input->post() && !$this->input->post('login')) {
        if ($this->input->post('delete_id')) $this->$model->delete($this->input->post('delete_id'));
        else $this->$model->save($this->input->post());
      }
      $data['thead'] = $this->$model->getThead();
    }
    $this->loadview('main', $data);
  }

  function download () {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachement; filename=document-template-loader.csv');
    ob_start();
    $file = fopen('php://output', 'w') or show_error("Can't open php://output");
    $this->load->model('Documentmodel');
    $form = $this->Documentmodel->getCSVForm();
    $fields= array();
    foreach ($form as $field) $fields[] = $field['label'];
    fwrite($file, '"sep=,"' . "\r\n");
    fputcsv($file, $fields);
    fclose($file) or show_error("Can't close php://output");
    $str = ob_get_contents();
    ob_end_clean();
    echo $str;
  }

  function export () {
    $fields = array();
    $header = array();
    $this->load->model('Documentmodel');
    $form = $this->Documentmodel->getAllForm();
    $this->db->select('document.*');
    $this->db->select('status.name status', false);
    $this->db->join('status', 'document.status = status.id', 'left');
    $records = $this->db->get('document')->result();

    foreach ($form as $field) $fields[$field['name']] = $field['label'];
    if (isset ($records[0])) foreach ($records[0] as $field => $value) $header[] = isset ($fields[$field]) ? $fields[$field] : $field;

    header('Content-Type: application/csv');
    header('Content-Disposition: attachement; filename=document-export.csv');
    ob_start();
    $file = fopen('php://output', 'w') or show_error("Can't open php://output");
    fwrite($file, '"sep=,"' . "\r\n");
    fputcsv($file, $header);
    foreach ($records as $record) fputcsv($file, (array)$record);
    fclose($file) or show_error("Can't close php://output");
    $str = ob_get_contents();
    ob_end_clean();
    echo $str;
  }

  function upload () {
    ini_set('auto_detect_line_endings',TRUE);
    $uploaded = $_FILES['loader']['tmp_name'];

    $csv= file_get_contents($uploaded);
    $delimiter   = array();
    $delimiter[','] = substr_count($csv, ',');
    $delimiter[';'] = substr_count($csv, ';');
    $delimiter['|'] = substr_count($csv, '|');
    asort($delimiter);
    end($delimiter);
    $delimiter = key($delimiter);

    $file = fopen($uploaded, 'r');
    while ($line = fgetcsv($file, 1000, $delimiter)) $lines[] = $line;
    fclose($file);
    ini_set('auto_detect_line_endings',FALSE);

    $this->load->model('Documentmodel');
    $labels = $this->Documentmodel->getLabels();
    $dates  = array();
    foreach ($this->Documentmodel->getDateFields() as $datef) $dates[] = $datef['name'];

    $column = array();
    foreach($lines[0] as $header) $column[] = $labels[$header];

    for ($l = 1; $l < count($lines); $l++) {
      $record = array();
      foreach ($lines[$l] as $c => $value) $record[$column[$c]] = $value;
      foreach ($dates as $date) if (isset ($record[$date])) $record[$date] = date("Y-m-d", strtotime($record[$date]));
      $this->Documentmodel->save($record);
    }
    redirect(site_url('Document/index'));
  }

  function mine ($json = false) {
    $data                 = array();
    $data['pagetype']     = 'table-document';
    $data['dataSource']   = site_url('document/filterByRole');
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $data['thead']        = array();
    $data['tbody']        = array();
    if (strlen($this->model) > 0) {
      $model = $this->model;
      $this->load->model($model);
      if ($this->input->post() && !$this->input->post('login')) {
        if ($this->input->post('delete_id')) $this->$model->delete($this->input->post('delete_id'));
        else $this->$model->save($this->input->post());
      }
      $data['thead'] = $this->$model->getThead();
    }
    $this->loadview('main', $data);
  }

  public function opendata () {
    $model = $this->model;
    $this->load->model($model);
    die ($this->$model->datatables($this->controller));
  }

  public function filterByRole (){
    if ($this->session->userdata('id') && strlen($this->model) > 0) {
      $model = $this->model;
      $this->load->model($model);
      echo $this->$model->filterByRole($this->controller);
    }
  }

  function read ($id, $complete = null) {
    $data                 = array();
    $data['pagetype']     = 'form';
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    if ('SUPERADMIN' !== $this->session->userdata('role_name')) $data['form_action'] = site_url ("$this->controller/mine");
    $model                = $this->model;
    $this->load->model($model);
    $data['form']         = null === $complete ? $this->$model->getForm($id) : $this->$model->getCompleteForm($id);
    $this->loadview('form', $data);
  }

  function delete_multiple () {
    $data                 = array();
    $data['pagetype']     = 'confirm';
    $data['controller']   = $this->controller;
    $data['page_title']   = $this->page_title;
    $data['page_caption'] = $this->page_caption;
    $data['table_title']  = $this->table_title;
    $data['table_caption']= $this->table_caption;
    $data['delete_id']    = $this->input->get('ids');
    $this->loadview('main', $data);
  }
}