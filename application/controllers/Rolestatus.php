<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rolestatus extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Rolestatus';
    $this->model        = 'Rolestatusmodel';
    $this->table_title  = 'Authentikasi Proses Pekerjaan';
    $this->table_caption= 'Pengaturan authentikasi proses pekerjaan sesuai bidang';
  }

}