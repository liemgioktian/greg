<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Role';
    $this->model        = 'Rolemodel';
    $this->table_title  = 'Bidang Administrator';
    $this->table_caption= 'Pengaturan bidang pekerjaan';
  }

}