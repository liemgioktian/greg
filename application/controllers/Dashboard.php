<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

  function __construct () {
    parent::__construct();
  }

  public function index()
  {
    $data                 = array();
    $data['pagetype']     = 'dashboard';
    $data['page_title']   = '';
    $data['page_caption'] = '';
    $data['table_title']  = 'Procurement Apps';
    $data['table_caption']= 'Aplikasi monitoring pengadaan';
    $this->loadview('main', $data);
  }

  function userguide () {
    $data                 = array();
    $data['pagetype']     = 'userguide';
    $data['page_title']   = '';
    $data['page_caption'] = '';
    $data['table_title']  = 'User Guide';
    $data['table_caption']= 'Panduan setup aplikasi bagi superadmin';
    $this->loadview('main', $data);
  }
}