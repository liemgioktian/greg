<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holiday extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'holiday';
    $this->model        = 'holidaymodel';
    $this->table_title  = 'Hari Libur';
    $this->table_caption= 'Pengaturan hari libur';
  }

}