<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

  function __construct () {
    parent::__construct();
    $this->controller   = 'Admin';
    $this->model        = 'Adminmodel';
    $this->table_title  = 'Administrator';
    $this->table_caption= 'Pengaturan user login dan pembagian authentikasi admin';
  }

  public function email()
  {
      $from_name = 'oyeah';
      $from_email = 'henry.castamere@gmail.com';
      $to = 'henri.susanto@indosystem.com';
      $subject = 'Yikky!';
      $message = 'nothing important';

      $this->load->library('email');
      $this->email->from($from_email, $from_name);
      $this->email->to($to);
      $this->email->subject($subject);
      $this->email->message($message);

      if (!$this->email->send()) echo $this->email->print_debugger();
      die;
  }

}