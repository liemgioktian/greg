<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rolestatusmodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'role_status';
    $this->thead = array(
      array('field' => 'role_name', 'label' => 'BIDANG ADMIN'),
      array('field' => 'status_name', 'label' => 'PROSES PEKERJAAN'),
    );
    $this->form  = array ();

    $this->load->model('Rolemodel');
    $roles  = $this->Rolemodel->find();
    $options= array();
    foreach ($roles as $role) $options[] = array('value' => $role->id, 'text' => $role->name);
    $this->form[]= array(
      'name'    => 'role',
      'label'   => 'Bidang Admin',
      'required'=> true,
      'options' => $options,
    );

    $this->load->model('Statusmodel');
    $status  = $this->Statusmodel->find();
    $options = array(array('value' => 0, 'text' => 'INPUT PEKERJAAN BARU'));
    foreach ($status as $stat) $options[] = array('value' => $stat->id, 'text' => $stat->name);
    $this->form[]= array(
      'name'    => 'status[]',
      'label'   => 'Status Pekerjaan',
      'required'=> true,
      'options'  => $options,
      'multiple'=> true
    );
  }

  function save ($data) {
    $statuss = '';
    foreach ($data['status'] as $status) $statuss .= strlen($statuss) < 1 ? $status : ",$status";
    $data['status'] = $statuss;
    return parent::save($data);
  }

  function find ($where = array()) {
    $this->db
      ->select('role_status.*')
      ->select('role.name as role_name', false)
      ->select('status.name as status_name', false)
      ->join('role', 'role_status.role = role.id')
      ->join('status', 'role_status.status = status.id');
    return parent::find($where);
  }

  function datatables ($controller){
    $this->load->library('datatables');
    $this->datatables->select('role.name as role_name');
    $this->datatables->select('GROUP_CONCAT(status.name SEPARATOR ", ") as status_name');
    $this->datatables->select('role_status.id');
    $this->datatables->edit_column('id', 
      '<a class="btn btn-xs btn-warning" href="'. site_url($controller . '/read/').'$1"><i class="fa fa-edit"></i></a>'.
      '<a class="btn btn-xs btn-danger" href="'. site_url($controller . '/delete/').'$1"><i class="fa fa-trash"></i></a>',
    'id');
    $this->datatables->from($this->table);
    $this->datatables
      ->join('role', 'role_status.role = role.id')
      ->join('status', 'FIND_IN_SET(status.id, role_status.status)');
    $this->datatables->group_by('role_status.id');
    return $this->datatables->generate();
  }

  function getStatus ($role = null) {
    $status = array();
    $role = is_null($role) ? $this->session->userdata('role') : $role;
    foreach (parent::find(array('role' => $role)) as $rs) foreach (explode(',', $rs->status) as $stat) $status[] = $stat;
    return $status;
  }

}