<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rolemodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'role';
    $this->thead = array(
      array('field' => 'name', 'label' => 'BIDANG'),
    );
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'name',
      'label'   => 'Bidang',
      'required'=> true
    );
  }

}
