<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holidaymodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'holiday';
    $this->thead = array(
      array('field' => 'holiday', 'label' => 'TANGGAL LIBUR'),
    );
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'holiday',
      'label'   => 'Tanggal Libur',
      'required'=> true,
      'type' => 'date'
    );
  }

  function add ($date, $days) {
    $holiday = array();
    foreach (parent::find() as $holi) $holiday[] = $holi->holiday;
    while ($days > 0) {
      $date = date('Y-m-d', strtotime($date . ' +1 weekday'));
      if (! in_array($date, $holiday)) $days--;
    }
    return $date;
  }

}
