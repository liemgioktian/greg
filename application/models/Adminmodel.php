<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'admin';
    $this->thead = array(
      array('field' => 'username', 'label' => 'NAMA'),
      array('field' => 'role_name', 'label' => 'BIDANG')
    );
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'username',
      'label'   => 'Name',
      'required'=> true
    );
    $this->form[]= array(
      'name' => 'password',
      'label'=> 'Password',
      'type' => 'password'
    );
    $this->form[]= array(
      'name' => 'retype_password',
      'label'=> 'Retype Password',
      'type' => 'password'
    );
    $this->load->model('Rolemodel');
    $roles  = $this->Rolemodel->find();
    $options= array();
    foreach ($roles as $role) $options[] = array('value' => $role->id, 'text' => $role->name);
    $this->form[]= array(
      'name'   => 'role',
      'label'   => 'Bidang',
      'options' => $options
    );
    $this->form[]= array(
      'name'    => 'email',
      'label'   => 'Email',
      'suffix'  => 'Untuk multiple email, pisahkan dengan koma (,)'
    );
    $this->form[]= array(
      'name'    => 'notified',
      'label'   => 'Notifikasi Waktu Pekerjaan',
      'options' => array(
        array('value' => '0', 'text' => 'Tidak Termasuk Penerima Notifikasi'),
        array('value' => '1', 'text' => 'Termasuk Penerima Notifikasi')
      )
    );
  }

  function login ($post) {
    if (!isset ($post['username'])) return false;
    if (!isset ($post['password'])) return false;
    $admin = $this->findOne(array(
      'username' => $post['username'],
      'password' => md5($post['password'])
    ));
    if (!$admin) return false;
    else {
      $this->load->model('Rolemodel');
      $role = $this->Rolemodel->findOne($admin['role']);
      $admin['role_name'] = $role['name'];
      $this->session->set_userdata($admin);
    }
    return true;
  }

  function find ($where = array()) {
    $this->db
      ->select('admin.*')
      ->select('role.name as role_name', false)
      ->join('role', 'admin.role = role.id', 'left');
    return parent::find($where);
  }

  function datatables ($controller){
    $this->load->library('datatables');
    $this->datatables->select('username');
    $this->datatables->select('role.name as role_name');
    $this->datatables->select('admin.id');
    $this->datatables->edit_column('id', 
      '<a class="btn btn-xs btn-warning" href="'. site_url($controller . '/read/').'$1"><i class="fa fa-edit"></i></a>'.
      '<a class="btn btn-xs btn-danger" href="'. site_url($controller . '/delete/').'$1"><i class="fa fa-trash"></i></a>',
    'id');
    $this->datatables->from($this->table);
    $this->datatables->join('role', 'admin.role = role.id', 'left');
    return $this->datatables->generate();
  }

  function create ($record) {
    $record['password'] = md5($record['password']);
    unset($record['retype_password']);
    return parent::create($record);
  }

  function update ($record) {
    if (strlen ($record['password']) > 0) $record['password'] = md5($record['password']);
    else unset($record['password']);
    unset($record['retype_password']);
    return parent::update($record);
  }

  function prepopulate ($id) {
    $record = $this->findOne($id);
    foreach ($this->form as $index => &$f)
      if (!in_array($f['name'], array('password', 'retype_password'))) $f['value'] = $record[$f['name']];
    return $this->form;
  }

  function validate ($input) {
    if ($input['password'] !== $input['retype_password']) return 'Password not match';
    return 'valid';
  }

  function getRecipients () {
    $records= parent::find(array('notified' => '1'));
    $emails = array();
    foreach ($records as $record) {
      if (strpos($record->email, ',') > -1) foreach (explode(',', $record->email) as $email) $emails[] = $email;
      else $emails[] = $record->email;
    }
    return $emails;
  }
}
