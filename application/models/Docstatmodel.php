<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docstatmodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'docstat';
    $this->load->model('holidaymodel');
  }

  function afterDocCreate ($docId, $statId) {
    foreach ($this->db->get('status')->result() as $stat) parent::create(array(
      'doc' => $docId,
      'stat'=> $stat->id,
    ));
    $this->onDocUpdateStatus($docId, $statId);
    $this->db->query("UPDATE $this->table SET `planned` = `calculated` WHERE `doc` = $docId");
  }

  function onDocUpdateStatus ($docId, $statId) {
    $this->db
    ->where(array('doc' => $docId))
    ->where(array('stat' => $statId))
    ->update($this->table, array('achieved' => date ('Y-m-d')));

    $doc = (object) $this->db->get_where('document', array ('id' => $docId))->row_array();
    $stat= (object) $this->db->get_where('status', array ('is_initial' => 1))->row_array();
    $reff = $stat->starting_field;
    $this->calc($doc, $stat, $doc->$reff);
  }

  private function calc ($doc, $stat, $reff, $done = array()) {
    $doc = !is_object ($doc) ? (object) $doc : $doc;
    $stat= !is_object ($stat) ? (object) $stat : $stat;
    $docstat = (object) $this->db->where(array('doc' => $doc->id, 'stat' => $stat->id))->get($this->table)->row_array();

    if ($stat->duration > 0) {}
    else if ('0' === $stat->duration) {}
    else {
      $dynamic_duration_field = $stat->duration;
      $stat->duration = $doc->$dynamic_duration_field;
    }
    $calculated = $this->holidaymodel->add($reff, $stat->duration);

    $this->db
    ->where(array('id' => $docstat->id))
    ->update($this->table, array('calculated' => $calculated));
    $done[] = $stat->id;

    foreach (explode(',', $stat->next) as $nextId) {
      $next = (object) $this->db->get_where('status', array('id' => $nextId))->row_array();
      $reff = $docstat->achieved;
      $reff = '0000-00-00' === $reff ? $calculated : $reff;
      if (isset ($next->id) && !in_array($next->id, $done)) $this->calc ($doc, $next, $reff, $done);
    }
  }

  function afterDeleteDoc ($docId) {
    $this->db->where(array('doc' => $docId))->delete($this->table);
  }

  function almostLate () {
    return $this->db->query("
      SELECT `document`.`name` as docname, `status`.`name` as statname, DATE_FORMAT(`calculated`, '%d %b %Y') as deadline
      FROM `document`
      LEFT JOIN `status` ON `document`.`status` = `status`.`id`
      LEFT JOIN `docstat` ON `document`.`id` = `docstat`.`doc` AND `document`.`status` = `docstat`.`stat`
      WHERE `docstat`.`planned` BETWEEN CURRENT_DATE() AND DATE_ADD(CURRENT_DATE(), INTERVAL 5 DAY)
    ")->result();
  }

}