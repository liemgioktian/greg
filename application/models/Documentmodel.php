<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentmodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'document';
    $this->thead = array(
      array('field' => 'checkbox', 'label' => ''),
      array('field' => 'name', 'label' => 'NAMA/JUDUL PEKERJAAN'),
      array('field' => 'jenis_anggaran', 'label' => 'ANGGARAN'),
      array('field' => 'status_name', 'label' => 'PROSES PEKERJAAN'),
      array('field' => 'next_status', 'label' => 'NEXT SCHEDULE'),
      array('field' => 'deadline', 'label' => 'DEADLINE'),
      array('field' => 'outdated', 'label' => 'PROSES TERLAMBAT'),
    );
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'name',
      'label'   => 'Nama/Judul Pekerjaan',
      'required'=> true
    );
    $this->form[]= array(
      'name'    => 'nomor_tor',
      'label'   => 'Nomor TOR',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'informasi_levering',
      'label'   => 'Informasi Levering',
      'type'    => 'text',
      'required'=> true
    );
    $this->form[]= array(
      'name'    => 'rab',
      'label'   => 'RAB Pekerjaan',
      'type'    => 'number'
    );
    $this->form[]= array(
      'name'    => 'jenis_anggaran',
      'label'   => 'Anggaran Pekerjaan',
      'options' => array(
        array('value' => '', 'text' => ''),
        array('value' => 'AO PJB - Jasa OM', 'text' => 'AO PJB - Jasa OM'),
        array('value' => 'AO PJB - K3', 'text' => 'AO PJB - K3'),
        array('value' => 'AO PJB - Kimia', 'text' => 'AO PJB - Kimia'),
        array('value' => 'AO PJB - Lingkungan', 'text' => 'AO PJB - Lingkungan'),
        array('value' => 'AO PJB - Preventive Maintenance', 'text' => 'AO PJB - Preventive Maintenance'),
        array('value' => 'AO PJB - Predictive Maintenance', 'text' => 'AO PJB - Predictive Maintenance'),
        array('value' => 'AO PJB - Corrective Maintenance', 'text' => 'AO PJB - Corrective Maintenance'),
        array('value' => 'AO PJB - Overhoul/Inspection', 'text' => 'AO PJB - Overhoul/Inspection'),
        array('value' => 'AO PJB - Engineering/Project/Modifikasi', 'text' => 'AO PJB - Engineering/Project/Modifikasi'),
        array('value' => 'AO PJB - Non Instalasi', 'text' => 'AO PJB - Non Instalasi'),
        array('value' => 'AI PJB', 'text' => 'AI PJB'),
        array('value' => 'AI PLN', 'text' => 'AI PLN'),
      )
    );
    $this->form[]= array(
      'name'    => 'plan_po',
      'label'   => 'Rencana Tanggal PO',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'real_po',
      'label'   => 'Tanggal PO',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'plan_penerimaan',
      'label'   => 'Target Penerimaan',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'plan_penagihan',
      'label'   => 'Target Penagihan',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'plan_tagihan_diterima',
      'label'   => 'Target Tagihan Diterima',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'plan_pembayaran',
      'label'   => 'Target Pembayaran',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'plan_tor_terbit',
      'label'   => 'Rencana Tanggal TOR Terbit',
      'type'    => 'date',
      'required'=> true
    );
    $this->form[]= array(
      'name'    => 'real_tor_terbit',
      'label'   => 'Tanggal TOR Terbit',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'no_pr',
      'label'   => 'Nomor PR',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'plan_terbit_pr',
      'label'   => 'Rencana Tanggal terbit PR',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'real_terbit_pr',
      'label'   => 'Tanggal terbit PR',
      'type'    => 'date'
    );

    $dok = array(
      'dok_pra' => 'Dokumen Pra',
      'surat_pemberitahuan_hasil_prakualifikasi' => 'Surat Pemberitahuan Hasil Prakualifikasi',
      'undangan_pengambilan_rks_pra' => 'Undangan Pengambilan RKS Pra',
      'bap_aanwijzing_lelang_pra' => 'BAP Aanwijzing Lelang Pra',
      'surat_penunjukan_pra' => 'Surat Penunjukan Pra',
      'kontrak_pra' => 'Kontrak Pra',
      'bap_aanwijzing_lelang_pasca' => 'BAP Aanwijzing Lelang Pasca',
      'surat_penunjukan_pasca' => 'Surat Penunjukan Pasca',
      'kontrak_pasca' => 'Kontrak Pasca',
      'pp_pengadaan_langsung' => 'PP Pengadaan Langsung',
      'kontrak_pengadaan_langsung' => 'Kontrak Pengadaan Langsung',
      'bap_aanwijzing_penunjukan_langsung' => 'BAP Aanwijzing Penunjukan Langsung',
      'surat_penunjukan_langsung' => 'Surat Penunjukan Langsung',
      'kontrak_penunjukan_langsung' => 'Kontrak Penunjukan Langsung',
    );
    foreach ($dok as $key => $value) {
      $this->form[]= array(
        'name'    => 'no_' . $key,
        'label'   => 'No. ' . $value,
        'type'    => 'text'
      );
      $this->form[]= array(
        'name'    => 'real_' . $key,
        'label'   => 'Tgl. ' . $value,
        'type'    => 'date'
      );
    }


    $this->form[]= array(
      'name'    => 'real_penyelesaian_pekerjaan',
      'label'   => 'Tgl Penyerahan Barang/Penyelesaian Pekerjaan',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'no_ba',
      'label'   => 'Nomor BA',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'real_ba',
      'label'   => 'Tanggal BA',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'no_bon_penerimaan',
      'label'   => 'Nomor Bon Penerimaan',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'real_bon_penerimaan',
      'label'   => 'Tanggal Bon Penerimaan',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'real_tagihan_diterima',
      'label'   => 'Tanggal Tagihan diterima',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'tagihan_diterima',
      'label'   => 'Tagihan Diterima',
      'type'    => 'number'
    );
    $this->form[]= array(
      'name'    => 'real_bayar',
      'label'   => 'Tanggal bayar',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'real_tagihan_supplier_diterima',
      'label'   => 'Tanggal Tagihan Supplier Diterima',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'ba_next',
      'label'   => 'Next',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'lelang_terbatas',
      'label'   => 'Lelang Terbatas',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'lelang_terbuka',
      'label'   => 'Lelang Terbuka',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'pengadaan_langsung',
      'label'   => 'Pengadaan Langsung',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'penunjukan_langsung',
      'label'   => 'Penunjukan Langsung',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'lelang_prakualifikasi',
      'label'   => 'Lelang Prakualifikasi',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'lelang_pascakualifikasi',
      'label'   => 'Lelang Pascakualifikasi',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'copy_kontrak',
      'label'   => 'Copy Kontrak',
      'options' => array(
        array('value' => '', 'text' => ''),
        array('value' => '0', 'text' => 'TIDAK'),
        array('value' => '1', 'text' => 'YA'),
      )
    );
    $this->form[]= array(
      'name'    => 'pemeriksaan_sesuai',
      'label'   => 'Pemeriksaan Sesuai',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'pemeriksaan_tidak_sesuai',
      'label'   => 'Pemeriksaan Tidak Sesuai',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'pemeriksaan_note',
      'label'   => 'Catatan Pemeriksaan Tidak Sesuai',
      'type'    => 'textarea'
    );
    $this->form[]= array(
      'name'    => 'tagihan_return',
      'label'   => 'Return',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'tagihan_verified',
      'label'   => 'Verified',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'tagihan_unverified',
      'label'   => 'Unverified',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'perlu_rks',
      'label'   => 'Perlu RKS',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'tidak_perlu_rks',
      'label'   => 'Tidak Perlu RKS',
      'type'    => 'button'
    );
    $this->form[]= array(
      'name'    => 'no_rks',
      'label'   => 'Nomor RKS',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'real_rks',
      'label'   => 'Tanggal RKS',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'no_hps_pra',
      'label'   => 'Nomor HPS Prakualifikasi',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'real_hps_pra',
      'label'   => 'Tanggal HPS Prakualifikasi',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'no_hps_pasca',
      'label'   => 'Nomor HPS Pascakualifikasi',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'real_hps_pasca',
      'label'   => 'Tanggal HPS Pascakualifikasi',
      'type'    => 'date'
    );
    $this->form[]= array(
      'name'    => 'no_hps_pl',
      'label'   => 'Nomor HPS Penunjukan Langsung',
      'type'    => 'text'
    );
    $this->form[]= array(
      'name'    => 'real_hps_pl',
      'label'   => 'Tanggal HPS  Penunjukan Langsung',
      'type'    => 'date'
    );
  }

  function getAllForm () {
    return $this->form;
  }

  function getDateFields () {
    $fields = array();
    foreach ($this->form as $field) {
      if (isset($field['type']) && 'date' === $field['type']) $fields[] = $field;
    }
    return $fields;
  }

  function getCompleteForm ($id = false) {
    $form = $id ? $this->prepopulate($id) : $this->form;

    if ($id) $form[] = array(
      'name' => 'id',
      'type' => 'hidden',
      'value'=> $id
    );

    foreach ($form as &$f) {
      if (isset ($f['options'])) $f['type'] = 'select';
      if (!isset ($f['type'])) $f['type']   = 'text';

      if (!isset ($f['value'])) $f['value']       = '';

      if (!isset ($f['required'])) $f['required'] = '';
      else $f['required'] = 'required="required"';

      $f['disabled'] = !isset($f['disabled']) ? '' : 'disabled="disabled"';
    }
    return $form;
  }

  function getCSVForm () {
    $csv = array();
    $labels = array('Nama/Judul Pekerjaan', 'Nomor TOR', 'Informasi Levering',
    'RAB Pekerjaan', 'Anggaran Pekerjaan',  'Rencana Tanggal TOR Terbit',
    'Rencana Tanggal terbit PR', 'Rencana Tanggal PO', 'Target Penerimaan');
    $fields = array();
    foreach ($this->form as $field) $fields[$field['label']] = $field;
    foreach ($labels as $label) $csv[] = $fields[$label];
    return $csv;
  }

  function getForm ($id = null) {
    $allfields = array();
    foreach ($this->form as $field) $allfields[$field['name']] = $field;
    $status = 0;
    if (!is_null ($id)) {
      $doc = $this->findOne($id);
      $status = $doc['status'];
    }

    $status = $this->db->get_where('status', array('id' => $status))->row_array();
    if ('1' === $status['is_final']) {
      foreach ($this->form as &$field) $field['disabled'] = true;
      return parent::getForm($id);
    } else $status = isset ($status['id']) ? $status['id'] : 0;

    $this->form= array();
    $fields = $this->db->get_where('field_appearance', array('status' => $status))->result();
    $fields_appear = array();
    foreach ($fields as $rec) {
      $fields_appear[] = $rec->field;
      if (strpos($rec->field, ',') < 0) $this->form[] = $allfields[$rec->field];
      else foreach (explode(',', $rec->field) as $field) $this->form[] = $allfields[$field];
    }
    if (!in_array('name', $fields_appear)) array_unshift($this->form, $allfields['name']);
    return parent::getForm($id);
  }

  function save ($data) {
    if (!isset ($data['name'])) return true;
    $tinyInt = array ('jenis_anggaran');
    foreach ($tinyInt as $field) if (isset($data[$field]) && strlen($data[$field]) < 1) unset($data[$field]);

    $dateFields = array();
    foreach ($this->getDateFields() as $df) $dateFields[] = $df['name'];
    foreach ($data as $field => &$value) if (in_array($field, $dateFields)) {
      $value = date("Y-m-d", strtotime($value));
    }

    return parent::save ($data);
  }

  function create ($data) {
    $this->load->model('Statusmodel');
    $init = $this->Statusmodel->findOne(array('is_initial' => '1'));
    if (isset ($init['id'])) $data['status'] = $init['id'];
    $data['last_status_update'] = date ('Y-m-d');
    $id = parent::create($data);
    $this->load->model('docstatmodel');
    $this->docstatmodel->afterDocCreate($id, $data['status']);
    return $id;
  }

  function update ($data) {
    $this->load->model('Fieldstatusmodel');
    $changers= $this->Fieldstatusmodel->find();
    $previous= $this->findOne($data['id']);

    foreach ($changers as $changer) {
      $is_target_status = false;
      $change = true;
      foreach (explode(',', $changer->field) as $field)
      {
        if (isset ($data[$field])) $is_target_status = true;
        $change *= (isset ($data[$field])
          && !$this->is_empty ($field, $data)
          && $this->is_empty($field, $previous))
        ||!$this->is_empty($field, $previous);
      }
      if ($change && $is_target_status) {
        $data['last_status_update'] = date ('Y-m-d');
        $data['status'] = $changer->status;
        $this->load->model('docstatmodel');
        parent::update($data);
        $this->docstatmodel->onDocUpdateStatus($data['id'], $data['status']);
      }
    }
    return $data['id'];//parent::update($data);
  }

  function is_empty ($name, $data) {
    $value= $data[$name];
    $form = $this->getAllForm();
    $type = 'text';
    foreach ($form as $field) if ($field['name'] == $name) $type = $field['type'];
    switch ($type) {
      case 'text': return strlen($value) < 1 ; break;
      case 'date': return '0000-00-00' == $value || '' == $value; break;
      case 'button': return $value == 0 ; break;
      case 'number': return $value <= 0 ; break;
    }
  }

  function get_value ($field, $previous, $data, $default = false) {
    $value = $default;
    if (isset($previous[$field]) && !$this->is_empty ($field, $previous)) $value = $previous[$field];
    if (isset($data[$field]) && !$this->is_empty ($field, $data)) $value = $data[$field];
    return $value;
  }

  function delete ($id) {
    $ids = array($id);
    if (strpos($id, ',') > -1) $ids = explode(',', $id);
    foreach ($ids as $id) $this->do_delete($id);
  }

  function do_delete ($id) {
    if ($this->session->userdata('role_name') !== 'SUPERADMIN') {
      $this->load->model('Statusmodel');
      $record = $this->findOne($id);
      $init   = $this->Statusmodel->findOne(array('is_initial' => '1'));
      if (isset ($init['id']) && $record['status'] !== $init['id']) return false;
    }
    $retval = parent::delete($id);
    $this->load->model('docstatmodel');
    $this->docstatmodel->afterDeleteDoc ($id);
    return $retval;
  }

  function find ($where = array()) {
    $this->db
      ->select('document.*')
      ->select('status.name as status_name')
      ->join('status', 'document.status = status.id', 'left');
    return parent::find($where);
  }

  function datatables ($controller, $astatus = array()){
    if (!empty ($astatus)) $this->db->where_in('document.status', $astatus);

    if ($this->input->post('columns')) {
      $colfields = ['', 'document.name', 'jenis_anggaran', 'status.name', 'next.name', 'deadline', 'outdated'];
      foreach ($this->input->post('columns') as $index => $column)
        if (isset ($column['search']) && isset ($column['search']['value']) && strlen($column['search']['value']) > 0)
          $this->db->like($colfields[$index], $column['search']['value']);
    }

    $this->load->library('datatables');

    $this->datatables->select('document.id checkbox', false);
    if ($this->session->userdata('role_name') === 'SUPERADMIN') $buttons = '<input type="checkbox" name="doc-check" value="$1">';
    else $buttons = '';
    $this->datatables->edit_column('checkbox', $buttons, 'id');

    $this->datatables->select('document.name');
    $this->datatables->select('jenis_anggaran');
    $this->datatables->select('status.name as status_name');
    $this->datatables->select('GROUP_CONCAT(next.name SEPARATOR " / ") as next_status');
    $this->datatables->select('DATE_FORMAT(currstat.calculated,"%d %b %Y") as deadline');
    $this->datatables->select("IF(GROUP_CONCAT(IF(docstat.calculated > docstat.planned, 'late', '') SEPARATOR '') LIKE '%late%' OR CURRENT_DATE() >= currstat.calculated, status.name, '') as outdated", false);

    $this->datatables->select('document.id');
    $buttons = '<a class="btn btn-xs btn-warning" data-action="edit-document" data-source="'. site_url($controller . '/read/').'$1"><i class="fa fa-edit"></i></a>'.
      '<a class="btn btn-xs btn-danger" href="'. site_url($controller . '/delete/').'$1"><i class="fa fa-trash"></i></a>';
    if ($this->session->userdata('role_name') === 'SUPERADMIN') $buttons .= '<a class="btn btn-xs btn-info" data-action="edit-document" data-source="'. site_url($controller . '/read/').'$1/correction"><i class="fa fa-eraser"></i></a>';
    else if (!$this->session->userdata('username') || empty ($astatus)) $buttons = '';
    $this->datatables->edit_column('id', $buttons, 'id');

    $this->datatables->from($this->table);
    $this->datatables->join('status', 'document.status = status.id', 'left');
    $this->datatables->join('status next', 'FIND_IN_SET(next.id, status.next)', 'left');
    $this->datatables->join('docstat', 'document.id = docstat.doc AND document.status = docstat.stat', 'left');
    $this->datatables->join('docstat currstat', 'document.id = currstat.doc AND document.status = currstat.stat', 'left');
    $this->datatables->group_by('document.id');

    $json = $this->datatables->generate();
    $dt = json_decode($json);
    $dt->data = 0 === $dt->recordsFiltered ? array() : $dt->data;
    return json_encode($dt);
  }

  function filterByRole ($controller) {
    $this->load->model('Rolestatusmodel');
    return $this->datatables ($controller, $this->Rolestatusmodel->getStatus());
  }

  function getLabels () {
    $labels = array();
    foreach ($this->form as $field) $labels[$field['label']] = $field['name'];
    return $labels;
  }

  function getTypes () {
    $types = array();
    foreach ($this->form as $field) $types[$field['name']] = isset($field['type']) ? $field['type'] : 'text';
    return $types;
  }
}
