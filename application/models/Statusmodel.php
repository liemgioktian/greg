<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statusmodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'status';
    $this->thead = array(
      array('field' => 'name', 'label' => 'PROSES PEKERJAAN'),
      array('field' => 'duration', 'label' => 'DURASI'),
      array('field' => 'next', 'label' => 'PROSES BERIKUTNYA'),
    );
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'name',
      'label'   => 'Proses Pekerjaan',
      'required'=> true
    );
    $this->form[]= array(
      'name'    => 'duration',
      'label'   => 'Durasi Maksimum',
    );
    $this->load->model('Documentmodel');
    $field   = $this->Documentmodel->getDateFields();
    $options = array(array('value' => '', 'text' => 'PROSES SEBELUMNYA'));
    foreach ($field as $f) $options[] = array('value' => $f['name'], 'text' => $f['label']);
    $this->form[]= array(
      'name'    => 'starting_field',
      'label'   => 'Acuan Deadline',
      'options' => $options,
    );
    $existings  = $this->find();
    $options= array(array('value' => 0, 'text' => ''));
    foreach ($existings as $exists) $options[] = array('value' => $exists->id, 'text' => $exists->name);
    $this->form[]= array(
      'name'    => 'next[]',
      'label'   => 'Proses Berikutnya',
      'options' => $options,
      'multiple'=> true
    );
    $suffix = 'Placeholder yang tersedia: <br/>';
    $this->load->model('Documentmodel');
    foreach ($this->Documentmodel->getLabels() as $label => $name) $suffix .= '{{' .$label. '}}, ';
    $suffix = substr($suffix, 0, -2);
    $this->form[]= array(
      'name'    => 'message',
      'label'   => 'Email Keterlambatan',
      'type'    => 'textarea',
      'suffix'  => $suffix
    );
  }

  function save ($data) {
    $exists = $this->find();
    if (count ($exists) < 1) $data['is_initial'] = '1';
    if (isset ($data['is_initial']) && '1' === $data['is_initial']) $this->db->update($this->table, array('is_initial' => 0));
    if (isset ($data['is_final']) && '1' === $data['is_final']) $this->db->update($this->table, array('is_final' => 0));
    if (isset ($data['next']) && is_array($data['next'])) $data['next'] = implode(',', $data['next']);
    return parent::save($data);
  }

  function datatables ($controller){
    $labels = array_flip($this->Documentmodel->getLabels());
    $this->load->library('datatables');
    $this->datatables->select('concat(status.name, IF(status.is_initial = 1, " (PROSES AWAL)", ""), IF(status.is_final = 1, " (PROSES AKHIR)", "")) as name');
    $this->datatables->select('concat(status.duration, CONCAT(" Hari Kerja", IF(LENGTH(status.starting_field) > 0, CONCAT(" Sejak ", status.starting_field), ""))) as duration');//IFNULL(status.starting_field, "", CONCAT(" SEJAK", status.starting_field))
    $this->datatables->select('next');
    $this->datatables->select('status.id');
    $this->datatables->edit_column('id', 
      '<a class="btn btn-xs btn-warning" href="'. site_url($controller . '/read/').'$1"><i class="fa fa-edit"></i></a>'.
      '<a class="btn btn-xs btn-danger" href="'. site_url($controller . '/delete/').'$1"><i class="fa fa-trash"></i></a>'.
      '<a class="btn btn-xs btn-info" href="'. site_url($controller . '/set_as_initial/').'$1"><i class="fa fa-star-o"></i></a>'.
      '<a class="btn btn-xs btn-info" href="'. site_url($controller . '/set_as_final/').'$1"><i class="fa fa-star"></i></a>',
    'id');
    $this->datatables->from($this->table);
    $dt = json_decode($this->datatables->generate());

    $stats = array();
    foreach (parent::find() as $stat) $stats[$stat->id] = $stat->name;

    foreach ($dt->data as &$row) {
      $fieldname = explode('Sejak', $row[1]);
      $row[1] = isset($fieldname[1]) ? str_replace($fieldname[1], ' ' . $labels[trim($fieldname[1])], $row[1]) : $row[1];

      $nexts = array();
      foreach (explode(',', $row[2]) as $id) $nexts[] = isset ($stats[$id]) ? $stats[$id] : '';
      $row[2] = implode(' / ', $nexts);
    }
    return json_encode($dt);
  }

  function delete ($id) {
    $exists = $this->find();
    if (count ($exists) <= 2) $this->db->update($this->table, array('is_initial' => 1));
    return parent::delete($id);
  }

  function getStartingFields () {
    $startings = array();
    foreach ($this->find() as $status) $startings[] = $status->starting_field;
    return $startings;
  }

  function cron () {
    $this->load->model(array('Adminmodel', 'Documentmodel'));
    $this->load->library('email');
    $recipients = $this->Adminmodel->getRecipients();
    $messages= array();
    $labels= $this->Documentmodel->getLabels();
    $types = $this->Documentmodel->getTypes();
    $query = "
      SELECT *,
      IF(GROUP_CONCAT(IF(docstat.calculated > docstat.planned, 'late', '') SEPARATOR '') LIKE '%late%' OR CURRENT_DATE() >= currstat.calculated, TRUE, FALSE) AS late
      FROM document
      LEFT JOIN docstat ON document.id = docstat.doc
      LEFT JOIN docstat currstat ON document.id = currstat.doc AND document.status = currstat.stat
    ";

    foreach (parent::find() as $status) $messages[$status->id] = $status->message;
    foreach ($this->db->query($query)->result() as $record) {
      if (0 == $record->late) continue;
      $message = $messages[$record->status];
      foreach ($labels as $label => $field) {
        $placeholder = '{{' .$label. '}}';
        $count = substr_count($message, $placeholder);
        if ('number' === $types[$field]) $record->$field = "Rp " . number_format($record->$field, 2, ',', '.');
        $message = str_replace($placeholder, $record->$field, $message, $count);
      }
      $subject = $record->name;
      $from = "PJB-UJTNY";

      foreach ($recipients as $recipient) {
        $this->email->from('eproc@ptpjb.com', $from);
        $this->email->to($recipient);
        $this->email->subject($subject);
        $this->email->message($message);
        if (!$this->email->send()) echo $this->email->print_debugger();
      }
    }
  }

}
