<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fieldstatusmodel extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'field_status';
    $this->thead = array(
      array('field' => 'status_name', 'label' => 'PROSES PEKERJAAN'),
      array('field' => 'label', 'label' => 'FIELD PEKERJAAN'),
    );
    $this->form  = array ();

    $this->load->model('Statusmodel');
    $statuss  = $this->Statusmodel->find();
    $options= array();
    foreach ($statuss as $status) $options[] = array('value' => $status->id, 'text' => $status->name);
    $this->form[]= array(
      'name'    => 'status',
      'label'   => 'Status Pekerjaan',
      'required'=> true,
      'options'  => $options,
    );

    $this->load->model('Documentmodel');
    $field   = $this->Documentmodel->getAllForm();
    $options = array();
    foreach ($field as $f) $options[] = array('value' => $f['name'], 'text' => $f['label']);
    $this->form[]= array(
      'name'    => 'field[]',
      'label'   => 'Field Pekerjaan',
      'required'=> true,
      'options' => $options,
      'multiple'=> true
    );
  }

  function save ($data) {
    $fields = '';
    foreach ($data['field'] as $field) $fields .= strlen($fields) < 1 ? $field : ",$field";
    $data['field'] = $fields;
    return parent::save($data);
  }

  function datatables ($controller){
    $this->load->library('datatables');
    $this->datatables->select('status.name as status_name');

    $vocab = array();
    $this->load->model('Documentmodel');
    foreach ($this->Documentmodel->getAllForm() as $field) $vocab[$field['name']] = $field['label'];

    $this->datatables->select('field as label');
    $this->datatables->select('field_status.id');
    $this->datatables->edit_column('id', 
      '<a class="btn btn-xs btn-warning" href="'. site_url($controller . '/read/').'$1"><i class="fa fa-edit"></i></a>'.
      '<a class="btn btn-xs btn-danger" href="'. site_url($controller . '/delete/').'$1"><i class="fa fa-trash"></i></a>',
    'id');
    $this->datatables->from($this->table);
    $this->datatables->join('status', 'field_status.status = status.id');
    $dt = json_decode($this->datatables->generate());
    foreach ($dt->data as &$row) {
      $label = '';
      if (strpos($row[1], ',')) {
        foreach (explode(',', $row[1]) as $field) $label .= strlen($label) < 1 ? $vocab[$field] : ', ' . $vocab[$field];
      } else $label = $vocab[$row[1]];
      $row[1] = $label;
    }
    return json_encode($dt);
  }
}