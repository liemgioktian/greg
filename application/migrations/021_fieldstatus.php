<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_fieldstatus extends CI_Migration {

  function up () {
    $this->db->query("ALTER TABLE `document` ADD `no_rks` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `real_rks` DATE NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `no_hps_pra` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `real_hps_pra` DATE NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `no_hps_pasca` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `real_hps_pasca` DATE NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `no_hps_pl` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `real_hps_pl` DATE NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `tagihan_unverified` TINYINT NOT NULL");
    $this->db->query("TRUNCATE `field_status`");
    $this->db->query("
      INSERT INTO `field_status` (`id`, `field`, `status`)
      VALUES
        (1,'nomor_tor,tidak_perlu_rks',28),
        (2,'nomor_tor,perlu_rks',27),
        (3,'no_pr,lelang_prakualifikasi',26),
        (4,'no_pr,lelang_pascakualifikasi',20),
        (5,'no_pr,penunjukan_langsung',14),
        (6,'no_pr,pengadaan_langsung',16),
        (7,'no_pr,lelang_prakualifikasi,no_rks,real_rks',26),
        (8,'no_pr,lelang_pascakualifikasi,no_rks,real_rks',20),
        (9,'no_pr,penunjukan_langsung,no_rks,real_rks',14),
        (10,'no_pr,pengadaan_langsung,no_rks,real_rks',16),
        (17,'no_hps_pra,real_hps_pra',25),
        (18,'no_surat_pemberitahuan_hasil_prakualifikasi,real_surat_pemberitahuan_hasil_prakualifikasi',24),
        (19,'no_undangan_pengambilan_rks_pra,real_undangan_pengambilan_rks_pra',23),
        (20,'no_bap_aanwijzing_lelang_pra,real_bap_aanwijzing_lelang_pra',22),
        (21,'no_surat_penunjukan_pra,real_surat_penunjukan_pra',21),
        (22,'no_kontrak_pra,real_kontrak_pra',10),
        (23,'no_hps_pasca,real_hps_pasca',19),
        (24,'no_bap_aanwijzing_lelang_pasca,real_bap_aanwijzing_lelang_pasca',18),
        (25,'no_surat_penunjukan_pasca,real_surat_penunjukan_pasca',17),
        (26,'no_kontrak_pasca,real_kontrak_pasca',10),
        (27,'no_pp_pengadaan_langsung,real_pp_pengadaan_langsung',15),
        (28,'no_kontrak_pengadaan_langsung,real_kontrak_pengadaan_langsung',10),
        (29,'no_hps_pl,real_hps_pl',13),
        (30,'no_bap_aanwijzing_penunjukan_langsung,real_bap_aanwijzing_penunjukan_langsung',12),
        (31,'no_surat_penunjukan_langsung,real_surat_penunjukan_langsung',11),
        (32,'no_kontrak_penunjukan_langsung,real_kontrak_penunjukan_langsung',10),
        (33,'informasi_levering',9),
        (34,'real_penyelesaian_pekerjaan',8),
        (35,'no_ba,real_ba',7),
        (36,'ba_next',6),
        (37,'pemeriksaan_tidak_sesuai',9),
        (38,'real_tagihan_supplier_diterima',5),
        (39,'tagihan_verified',4),
        (40,'tagihan_unverified',6),
        (41,'real_bayar',3)
    ");
    $this->db->query("TRUNCATE `field_appearance`");
    $this->db->query("
      INSERT INTO `field_appearance` (`id`, `field`, `status`)
      VALUES
        (1,'plan_tor_terbit',0),
        (2,'nomor_tor,perlu_rks,tidak_perlu_rks',29),
        (3,'no_pr,lelang_terbatas,lelang_terbuka,pengadaan_langsung,penunjukan_langsung,lelang_prakualifikasi,lelang_pascakualifikasi',28),
        (4,'no_pr,lelang_terbatas,lelang_terbuka,pengadaan_langsung,penunjukan_langsung,lelang_prakualifikasi,lelang_pascakualifikasi,no_rks,real_rks',27),
        (5,'no_hps_pra,real_hps_pra',26),
        (6,'no_surat_pemberitahuan_hasil_prakualifikasi,real_surat_pemberitahuan_hasil_prakualifikasi',25),
        (7,'no_undangan_pengambilan_rks_pra,real_undangan_pengambilan_rks_pra',24),
        (8,'no_bap_aanwijzing_lelang_pra,real_bap_aanwijzing_lelang_pra',23),
        (9,'no_surat_penunjukan_pra,real_surat_penunjukan_pra',22),
        (10,'no_kontrak_pra,real_kontrak_pra',21),
        (11,'no_hps_pasca,real_hps_pasca',20),
        (12,'no_bap_aanwijzing_lelang_pasca,real_bap_aanwijzing_lelang_pasca',19),
        (13,'no_surat_penunjukan_pasca,real_surat_penunjukan_pasca',18),
        (14,'no_kontrak_pasca,real_kontrak_pasca',17),
        (15,'no_pp_pengadaan_langsung,real_pp_pengadaan_langsung',16),
        (16,'no_kontrak_pengadaan_langsung,real_kontrak_pengadaan_langsung',15),
        (17,'no_hps_pl,real_hps_pl',14),
        (18,'no_bap_aanwijzing_penunjukan_langsung,real_bap_aanwijzing_penunjukan_langsung',13),
        (19,'no_surat_penunjukan_langsung,real_surat_penunjukan_langsung',12),
        (20,'no_kontrak_penunjukan_langsung,real_kontrak_penunjukan_langsung',11),
        (21,'informasi_levering',10),
        (22,'real_penyelesaian_pekerjaan,pemeriksaan_note',9),
        (23,'no_ba,real_ba',8),
        (24, 'ba_next', 7),
        (25,'real_tagihan_supplier_diterima',6),
        (26,'tagihan_verified,tagihan_unverified',5),
        (27,'real_bayar',4)
    ");
  }

  function down () {
    $this->db->query("ALTER TABLE `document` DROP `no_rks`");
    $this->db->query("ALTER TABLE `document` DROP `real_rks`");
    $this->db->query("ALTER TABLE `document` DROP `no_hps_pra`");
    $this->db->query("ALTER TABLE `document` DROP `real_hps_pra`");
    $this->db->query("ALTER TABLE `document` DROP `no_hps_pasca`");
    $this->db->query("ALTER TABLE `document` DROP `real_hps_pasca`");
    $this->db->query("ALTER TABLE `document` DROP `no_hps_pl`");
    $this->db->query("ALTER TABLE `document` DROP `real_hps_pl`");
    $this->db->query("ALTER TABLE `document` DROP `tagihan_unverified`");
    $this->db->query("TRUNCATE `field_status`");
    $this->db->query("TRUNCATE `field_appearance`");
  }

}