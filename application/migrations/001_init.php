<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_init extends CI_Migration {

  public function up()
  {
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `admin` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `role` int(11) NOT NULL,
        `username` varchar(100) NOT NULL,
        `password` varchar(128) NOT NULL,
        KEY `id` (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `document` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `status` int(11) NOT NULL,
        `name` varchar(255) NOT NULL DEFAULT '',
        `nomor_tor` varchar(255) NOT NULL,
        `informasi_levering` text NOT NULL,
        `rab` int(11) NOT NULL,
        `jenis_anggaran` tinyint(4) DEFAULT NULL,
        `is_hps` tinyint(4) DEFAULT NULL,
        `nomor_pr` varchar(255) NOT NULL,
        `metode_pengadaan` varchar(255) NOT NULL DEFAULT '',
        `plan_tor` date NOT NULL,
        `plan_hps_pr` date NOT NULL,
        `plan_contract` date NOT NULL,
        `plan_levering` date NOT NULL,
        `plan_disburse` date NOT NULL,
        `real_tor` date NOT NULL,
        `real_hps_pr` date NOT NULL,
        `real_contract` date NOT NULL,
        `real_levering` date NOT NULL,
        `real_disburse` date NOT NULL,
        PRIMARY KEY (`id`),
        KEY `status` (`status`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `field_role` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `role` int(11) NOT NULL,
        `field` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `field_status` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `field` varchar(255) NOT NULL,
        `status` int(11) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `role` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `role_status` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `status` int(11) NOT NULL,
        `role` int(11) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `status` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `is_initial` tinyint(4) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    
    $this->db->query("REPLACE INTO `role` (`id`, `name`) VALUES (1, 'SUPERADMIN')");
    $this->db->query("REPLACE INTO `admin` (`id`, `role`, `username`, `password`) VALUES (1, 1, 'superadmin', '202cb962ac59075b964b07152d234b70')");
  }

  public function down () {
    foreach (array('admin', 'document', 'field_role', 'field_status', 'role', 'role_status', 'status') as $table) {
      $this->db->query("DROP TABLE IF EXISTS $table");
    }
  }

}