<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_finalstate extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `status` ADD `is_final` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_initial`");
  }

  public function down () {
    // $this->db->query("ALTER TABLE `status` DROP `is_final`");
  }
}