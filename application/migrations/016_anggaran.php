<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_anggaran extends CI_Migration {

  public function up() {
    $this->db->query("ALTER TABLE `document` CHANGE `jenis_anggaran` `jenis_anggaran` VARCHAR(255) NULL DEFAULT NULL");
    $this->db->query("UPDATE document SET jenis_anggaran = 'AO' WHERE jenis_anggaran = '0'");
    $this->db->query("UPDATE document SET jenis_anggaran = 'AI' WHERE jenis_anggaran = '1'");
  }

  public function down () {
    $this->db->query("UPDATE document SET jenis_anggaran = '0' WHERE jenis_anggaran = 'AO'");
    $this->db->query("UPDATE document SET jenis_anggaran = '1' WHERE jenis_anggaran = 'AI'");
    $this->db->query("ALTER TABLE `document` CHANGE `jenis_anggaran` `jenis_anggaran` tinyint(4) DEFAULT NULL");
  }

}