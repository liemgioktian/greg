<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_docfield extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document`
      DROP `is_hps`,
      DROP `nomor_pr`,
      DROP `metode_pengadaan`,
      DROP `plan_tor`,
      DROP `plan_hps_pr`,
      DROP `plan_contract`,
      DROP `plan_levering`,
      DROP `plan_disburse`,
      DROP `real_tor`,
      DROP `real_hps_pr`,
      DROP `real_contract`,
      DROP `real_levering`,
      DROP `real_disburse`");
    $this->db->query("ALTER TABLE `document`  ADD `real_pprks` DATE NOT NULL  AFTER `jenis_anggaran`,  ADD `real_po` DATE NOT NULL  AFTER `real_pprks`,  ADD `plan_penerimaan` DATE NOT NULL  AFTER `real_po`,  ADD `plan_penagihan` DATE NOT NULL  AFTER `plan_penerimaan`,  ADD `plan_tagihan_diterima` DATE NOT NULL  AFTER `plan_penagihan`,  ADD `plan_pembayaran` DATE NOT NULL  AFTER `plan_tagihan_diterima`,  ADD `real_tor_terbit` DATE NOT NULL  AFTER `plan_pembayaran`,  ADD `real_tor_diterima` DATE NOT NULL  AFTER `real_tor_terbit`,  ADD `no_pr` VARCHAR(255) NOT NULL  AFTER `real_tor_diterima`,  ADD `real_terbit_pr` DATE NOT NULL  AFTER `no_pr`,  ADD `no_hps` VARCHAR(255) NOT NULL  AFTER `real_terbit_pr`,  ADD `nilai_hps` INT NOT NULL  AFTER `no_hps`,  ADD `no_dok_pra` VARCHAR(255) NOT NULL  AFTER `nilai_hps`,  ADD `real_dok_pra` DATE NOT NULL  AFTER `no_dok_pra`,  ADD `no_surat_pemberitahuan_hasil_prakualifikasi` VARCHAR(255) NOT NULL  AFTER `real_dok_pra`,  ADD `real_surat_pemberitahuan_hasil_prakualifikasi` DATE NOT NULL  AFTER `no_surat_pemberitahuan_hasil_prakualifikasi`,  ADD `no_undangan_pengambilan_rks_pra` VARCHAR(255) NOT NULL  AFTER `real_surat_pemberitahuan_hasil_prakualifikasi`,  ADD `real_undangan_pengambilan_rks_pra` DATE NOT NULL  AFTER `no_undangan_pengambilan_rks_pra`,  ADD `no_bap_aanwijzing_lelang_pra` VARCHAR(255) NOT NULL  AFTER `real_undangan_pengambilan_rks_pra`,  ADD `real_bap_aanwijzing_lelang_pra` DATE NOT NULL  AFTER `no_bap_aanwijzing_lelang_pra`,  ADD `no_surat_penunjukan_pra` VARCHAR(255) NOT NULL  AFTER `real_bap_aanwijzing_lelang_pra`,  ADD `real_surat_penunjukan_pra` DATE NOT NULL  AFTER `no_surat_penunjukan_pra`,  ADD `no_kontrak_pra` VARCHAR(255) NOT NULL  AFTER `real_surat_penunjukan_pra`,  ADD `real_kontrak_pra` INT NOT NULL  AFTER `no_kontrak_pra`,  ADD `no_rks_lelang_pasca` VARCHAR(255) NOT NULL  AFTER `real_kontrak_pra`,  ADD `real_rks_lelang_pasca` DATE NOT NULL  AFTER `no_rks_lelang_pasca`,  ADD `no_bap_aanwijzing_lelang_pasca` VARCHAR(255) NOT NULL  AFTER `real_rks_lelang_pasca`,  ADD `real_bap_aanwijzing_lelang_pasca` DATE NOT NULL  AFTER `no_bap_aanwijzing_lelang_pasca`,  ADD `no_surat_penunjukan_pasca` VARCHAR(255) NOT NULL  AFTER `real_bap_aanwijzing_lelang_pasca`,  ADD `real_surat_penunjukan_pasca` DATE NOT NULL  AFTER `no_surat_penunjukan_pasca`,  ADD `no_kontrak_pasca` VARCHAR(255) NOT NULL  AFTER `real_surat_penunjukan_pasca`,  ADD `real_kontrak_pasca` DATE NOT NULL  AFTER `no_kontrak_pasca`,  ADD `no_pp_pengadaan_langsung` VARCHAR(255) NOT NULL  AFTER `real_kontrak_pasca`,  ADD `real_pp_pengadaan_langsung` DATE NOT NULL  AFTER `no_pp_pengadaan_langsung`,  ADD `no_kontrak_pengadaan_langsung` VARCHAR(255) NOT NULL  AFTER `real_pp_pengadaan_langsung`,  ADD `real_kontrak_pengadaan_langsung` DATE NOT NULL  AFTER `no_kontrak_pengadaan_langsung`,  ADD `no_rks_penunjukan_langsung` VARCHAR(255) NOT NULL  AFTER `real_kontrak_pengadaan_langsung`,  ADD `real_rks_penunjukan_langsung` DATE NOT NULL  AFTER `no_rks_penunjukan_langsung`,  ADD `no_bap_aanwijzing_penunjukan_langsung` VARCHAR(255) NOT NULL  AFTER `real_rks_penunjukan_langsung`,  ADD `real_bap_aanwijzing_penunjukan_langsung` DATE NOT NULL  AFTER `no_bap_aanwijzing_penunjukan_langsung`,  ADD `no_surat_penunjukan_langsung` VARCHAR(255) NOT NULL  AFTER `real_bap_aanwijzing_penunjukan_langsung`,  ADD `real_surat_penunjukan_langsung` DATE NOT NULL  AFTER `no_surat_penunjukan_langsung`,  ADD `no_kontrak_penunjukan_langsung` VARCHAR(255) NOT NULL  AFTER `real_surat_penunjukan_langsung`,  ADD `real_kontrak_penunjukan_langsung` DATE NOT NULL  AFTER `no_kontrak_penunjukan_langsung`,  ADD `real_penyelesaian_pekerjaan` DATE NOT NULL  AFTER `real_kontrak_penunjukan_langsung`,  ADD `no_ba` VARCHAR(255) NOT NULL  AFTER `real_penyelesaian_pekerjaan`,  ADD `real_ba` DATE NOT NULL  AFTER `no_ba`,  ADD `no_bon_penerimaan` VARCHAR(255) NOT NULL  AFTER `real_ba`,  ADD `real_bon_penerimaan` DATE NOT NULL  AFTER `no_bon_penerimaan`,  ADD `real_tagihan_diterima` DATE NOT NULL  AFTER `real_bon_penerimaan`,  ADD `tagihan_diterima` INT NOT NULL  AFTER `real_tagihan_diterima`,  ADD `real_bayar` DATE NOT NULL  AFTER `tagihan_diterima`;");
  }

  public function down () {
    
  }

}