<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_rebuildstatus extends CI_Migration {

  public function up() {
    $this->db->query("TRUNCATE `status`");
    $this->db->query("ALTER TABLE `status` CHANGE `next` `next` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `status` ADD `message` TEXT NOT NULL");
    $this->db->query("
      INSERT INTO `status` (`id`, `name`, `is_initial`, `is_final`, `next`, `duration`, `starting_field`, `message`) VALUES
      (3, 'SELESAI', 0, 1, '', 0, '', ''),
      (4, 'VERIFIKASI DAN PEMBAYARAN', 0, 0, '3', 14, '', ''),
      (5, 'VERIFIKASI TAGIHAN SUPPLIER', 0, 0, '4,6', 3, '', ''),
      (6, 'MENUNGGU TAGIHAN SUPPLIER', 0, 0, '5', 0, '', ''),
      (7, 'TERBIT BA', 0, 0, '6,9', 0, '', ''),
      (8, 'BARANG DATANG / PEKERJAAN SELESAI', 0, 0, '7', 14, '', ''),
      (9, 'LEVERING', 0, 0, '8', 0, 'informasi_levering', ''),
      (10, 'MENUNGGU INPUT LEVERING', 0, 0, '9', 1, '', ''),
      (11, 'PENERBITAN KONTRAK PENUNJUKAN LANGSUNG', 0, 0, '10', 5, '', ''),
      (12, 'PENUNJUKAN - PENUNJUKAN LANGSUNG', 0, 0, '11', 8, '', ''),
      (13, 'AANWIJZING PENUNJUKAN LANGSUNG', 0, 0, '12', 6, '', ''),
      (14, 'HPS PENUNJUKAN LANGSUNG', 0, 0, '13', 7, '', ''),
      (15, 'PENERBITAN KONTRAK PENGADAAN LANGSUNG', 0, 0, '10', 6, '', ''),
      (16, 'PP SUPPLIER', 0, 0, '15', 6, '', ''),
      (17, 'PENERBITAN KONTRAK PASCAKUALIFIKASI', 0, 0, '10', 19, '', ''),
      (18, 'PENUNJUKAN PASCAKUALIFIKASI', 0, 0, '17', 23, '', ''),
      (19, 'AANWIJZING LELANG PASCAKUALIFIKASI', 0, 0, '18', 7, '', ''),
      (20, 'HPS PASCAKUALIFIKASI', 0, 0, '19', 5, '', ''),
      (21, 'PENERBITAN KONTRAK PRAKUALIFIKASI', 0, 0, '10', 19, '', ''),
      (22, 'PENUNJUKAN PRAKUALIFIKASI', 0, 0, '21', 12, '', ''),
      (23, 'AANWIJZING LELANG PRAKUALIFIKASI', 0, 0, '22', 5, '', ''),
      (24, 'UNDANGAN PENGAMBILAN RKS', 0, 0, '23', 14, '', ''),
      (25, 'SURAT PEMBERITAHUAN HASIL PRAKUALIFIKASI', 0, 0, '24', 14, '', ''),
      (26, 'HPS PRAKUALIFIKASI', 0, 0, '25', 5, '', ''),
      (27, 'WAITING RKS', 0, 0, '14,16,20,26', 5, '', ''),
      (28, 'INPUT NOMOR PR', 0, 0, '14,16,20,26', 1, '', ''),
      (29, 'WAITING TOR', 1, 0, '27,28', 0, 'plan_tor_terbit', '')
    ");
  }

  public function down () {
    $this->db->query("ALTER TABLE `status` CHANGE `next` `next` INT(11) NOT NULL");
    $this->db->query("ALTER TABLE `status` DROP `message`");
  }

}