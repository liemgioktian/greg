<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_bugfixrealkontrakpra extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document` CHANGE `real_kontrak_pra` `real_kontrak_pra` DATE NOT NULL");
  }

  public function down () {
    $this->db->query("ALTER TABLE `document` CHANGE `real_kontrak_pra` `real_kontrak_pra` INT NOT NULL");
  }
}