<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_docstat extends CI_Migration {

  function up () {
    $this->db->query("
      CREATE TABLE `docstat` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `doc` int(11) NOT NULL,
        `stat` int(11) NOT NULL,
        `planned` date NOT NULL,
        `calculated` date NOT NULL,
        `achieved` date NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  function down () {
    $this->db->query('DROP TABLE IF EXISTS `docstat`');
  }
}