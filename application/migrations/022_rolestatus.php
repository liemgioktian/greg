<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_rolestatus extends CI_Migration {

  function up () {
    $this->db->query("TRUNCATE `role_status`");
    $this->db->query("ALTER TABLE `role_status` CHANGE `status` `status` VARCHAR(255) NOT NULL");
    $this->db->query("
      INSERT INTO `role_status` (`id`, `status`, `role`) VALUES
      (2, '0,16,27,28,29', 2),
      (3, '11,12,13,14,15,17,18,19,20,21,22,23,24,25,26', 3),
      (4, '7,8,9,10', 4),
      (5, '5,6', 3),
      (6, '4', 5)
    ");
  }

  function down () {
    $this->db->query("ALTER TABLE `role_status` CHANGE `status` `status` INT(11) NOT NULL");
  }

}