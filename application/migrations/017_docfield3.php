<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_docfield3 extends CI_Migration {

  function up () {
    $this->db->query("
      ALTER TABLE `document`
        DROP `real_pprks`,
        DROP `no_hps`,
        DROP `nilai_hps`,
        DROP `no_rks_lelang_pasca`,
        DROP `real_rks_lelang_pasca`,
        DROP `no_rks_penunjukan_langsung`,
        DROP `real_rks_penunjukan_langsung`,
        DROP `perlu_hps`,
        DROP `tidak_perlu_hps`,
        DROP `plan_pprks`
    ");
    $this->db->query("ALTER TABLE `document` ADD `perlu_rks` TINYINT NOT NULL, ADD `tidak_perlu_rks` TINYINT NOT NULL");
  }

  function down () {
    $this->db->query("
      ALTER TABLE `document`
        ADD `real_pprks` DATE,
        ADD `no_hps` VARCHAR(255),
        ADD `nilai_hps` INT,
        ADD `no_rks_lelang_pasca` VARCHAR(255),
        ADD `real_rks_lelang_pasca` DATE,
        ADD `no_rks_penunjukan_langsung` VARCHAR(255),
        ADD `real_rks_penunjukan_langsung` DATE,
        ADD `perlu_hps` TINYINT,
        ADD `tidak_perlu_hps` TINYINT,
        ADD `plan_pprks` DATE
    ");
    $this->db->query("ALTER TABLE `document` DROP `perlu_rks`, DROP `tidak_perlu_rks`");
  }

}