<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_fieldappearance extends CI_Migration {

  public function up()
  {
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `field_appearance` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `field` varchar(255) NOT NULL,
        `status` int(11) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("DROP TABLE `field_role`");
  }

  public function down () {
    $this->db->query("DROP TABLE `field_appearance`"); 
  }
}