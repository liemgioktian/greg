<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_dynamicduration extends CI_Migration {

  function up () {
    $this->db->query("ALTER TABLE `status` CHANGE `duration` `duration` VARCHAR(255) NOT NULL");
    $this->db->query("UPDATE `status` SET `starting_field` = '', `duration` = 'informasi_levering' WHERE `id` = 9");
  }

  function down () {
    $this->db->query("UPDATE `status` SET `starting_field` = 'informasi_levering', `duration` = 0 WHERE `id` = 9");
    $this->db->query("ALTER TABLE `status` CHANGE `duration` `duration` INT(11) NOT NULL");
  }

}