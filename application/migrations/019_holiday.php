<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_holiday extends CI_Migration {

  function up () {
    $this->db->query("
      CREATE TABLE `holiday` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `holiday` date NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  function down () {
    $this->db->query("DROP TABLE IF EXISTS `holiday`");
  }

}