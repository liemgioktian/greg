<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_docfield2 extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document` ADD `real_tagihan_supplier_diterima` DATE NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `ba_next` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `perlu_hps` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `tidak_perlu_hps` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `lelang_terbatas` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `lelang_terbuka` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `pengadaan_langsung` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `penunjukan_langsung` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `lelang_prakualifikasi` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `lelang_pascakualifikasi` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `copy_kontrak` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `pemeriksaan_sesuai` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `pemeriksaan_tidak_sesuai` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `tagihan_return` TINYINT(1) NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `tagihan_verified` TINYINT(1) NOT NULL");
    $this->db->query("
      INSERT INTO `status` (`id`, `name`, `is_initial`, `is_final`) VALUES
      (1, 'WAITING TOR', 1, 0),
      (2, 'TOR DITERIMA', 0, 0),
      (3, 'WAITING HPS', 0, 0),
      (4, 'INPUT NOMOR PR', 0, 0),
      (5, 'PR', 0, 0),
      (6, 'LELANG TERBATAS', 0, 0),
      (7, 'LELANG TERBUKA', 0, 0),
      (12, 'PP', 0, 0),
      (13, 'DOKUMEN PRAKUALIFIKASI', 0, 0),
      (14, 'RKS LELANG', 0, 0),
      (15, 'KONTRAK PENGADAAN LANGSUNG', 0, 0),
      (16, 'RKS PENUNJUKAN LANGSUNG', 0, 0),
      (17, 'SURAT PEMBERITAHUAN HASIL PRAKUALIFIKASI', 0, 0),
      (18, 'AANWIJZING LELANG PASCAKUALIFIKASI', 0, 0),
      (19, 'AANWIJZING PENUNJUKAN LANGSUNG', 0, 0),
      (20, 'UNDANGAN PENGAMBILAN RKS', 0, 0),
      (21, 'PENUNJUKAN LELANG PASCAKUALIFIKASI', 0, 0),
      (22, 'PENUNJUKAN (PENUNJUKAN LANGSUNG)', 0, 0),
      (23, 'AANWIJZING LELANG PRAKUALIFIKASI', 0, 0),
      (24, 'PENUNJUKAN LELANG PRAKUALIFIKASI', 0, 0),
      (25, 'PENERBITAN KONTRAK LELANG PASCAKUALIFIKASI', 0, 0),
      (26, 'PENERBITAN KONTRAK LELANG PRAKUALIFIKASI', 0, 0),
      (27, 'PENERBITAN KONTRAK PENUNJUKAN LANGSUNG', 0, 0),
      (29, 'BARANG DATANG / PEKERJAAN SELESAI', 0, 0),
      (30, 'INFORMASI PENGADAAN', 0, 0),
      (31, 'TERBIT BA', 0, 0),
      (32, 'MENUNGGU TAGIHAN SUPPLIER', 0, 0),
      (33, 'VERIFIKASI TAGIHAN SUPPLIER', 0, 0),
      (35, 'VERIFIKASI DAN PEMBAYARAN', 0, 0),
      (36, 'SELESAI', 0, 1),
      (37, 'LEVERING', 0, 0)
    ");
    $this->db->query("
      INSERT INTO `field_appearance` (`id`, `field`, `status`) VALUES
      (3, 'perlu_hps,tidak_perlu_hps', 2),
      (6, 'lelang_terbatas,lelang_terbuka,pengadaan_langsung,penunjukan_langsung', 5),
      (7, 'lelang_prakualifikasi,lelang_pascakualifikasi', 6),
      (8, 'lelang_prakualifikasi,lelang_pascakualifikasi', 7),
      (9, 'no_dok_pra,real_dok_pra', 13),
      (10, 'no_pp_pengadaan_langsung,real_pp_pengadaan_langsung', 12),
      (11, 'no_rks_lelang_pasca,real_rks_lelang_pasca', 14),
      (12, 'no_rks_penunjukan_langsung,real_rks_penunjukan_langsung', 16),
      (13, 'no_kontrak_pengadaan_langsung,real_kontrak_pengadaan_langsung', 15),
      (14, 'no_surat_pemberitahuan_hasil_prakualifikasi,real_surat_pemberitahuan_hasil_prakualifikasi', 17),
      (15, 'no_bap_aanwijzing_lelang_pasca,real_bap_aanwijzing_lelang_pasca', 18),
      (16, 'no_bap_aanwijzing_penunjukan_langsung,real_bap_aanwijzing_penunjukan_langsung', 19),
      (17, 'no_undangan_pengambilan_rks_pra,real_undangan_pengambilan_rks_pra', 20),
      (18, 'no_surat_penunjukan_pasca,real_surat_penunjukan_pasca', 21),
      (19, 'no_surat_penunjukan_langsung,real_surat_penunjukan_langsung', 22),
      (20, 'no_bap_aanwijzing_lelang_pra,real_bap_aanwijzing_lelang_pra', 23),
      (21, 'no_kontrak_pasca,real_kontrak_pasca', 25),
      (22, 'no_kontrak_penunjukan_langsung,real_kontrak_penunjukan_langsung', 27),
      (23, 'no_surat_penunjukan_pra,real_surat_penunjukan_pra', 24),
      (24, 'no_kontrak_pra,real_kontrak_pra', 26),
      (29, 'ba_next', 31),
      (30, 'real_tagihan_supplier_diterima', 32),
      (32, 'tagihan_return,tagihan_verified', 33),
      (33, 'real_tagihan_diterima,real_bayar', 35),
      (39, 'informasi_levering,rab,plan_tor_terbit', 0),
      (40, 'rab', 2),
      (41, 'real_tor_terbit', 1),
      (42, 'no_pr,real_terbit_pr,no_hps,nilai_hps', 3),
      (43, 'no_pr,real_terbit_pr', 4),
      (44, 'informasi_levering', 30),
      (45, 'real_penyelesaian_pekerjaan', 37),
      (46, 'no_ba,real_ba,pemeriksaan_sesuai,pemeriksaan_tidak_sesuai,pemeriksaan_note', 29)
    ");
    $this->db->query("
      INSERT INTO `field_status` (`id`, `field`, `status`) VALUES
      (3, 'perlu_hps', 3),
      (4, 'tidak_perlu_hps', 4),
      (7, 'lelang_terbatas', 6),
      (8, 'lelang_terbuka', 7),
      (9, 'pengadaan_langsung', 12),
      (10, 'penunjukan_langsung', 16),
      (11, 'lelang_prakualifikasi', 13),
      (12, 'lelang_pascakualifikasi', 14),
      (13, 'no_pp_pengadaan_langsung,real_pp_pengadaan_langsung', 15),
      (14, 'no_dok_pra,real_dok_pra', 17),
      (15, 'no_rks_lelang_pasca,real_rks_lelang_pasca', 18),
      (16, 'no_rks_penunjukan_langsung,real_rks_penunjukan_langsung', 19),
      (17, 'no_surat_pemberitahuan_hasil_prakualifikasi,real_surat_pemberitahuan_hasil_prakualifikasi', 20),
      (18, 'no_bap_aanwijzing_lelang_pasca,real_bap_aanwijzing_lelang_pasca', 21),
      (19, 'no_bap_aanwijzing_penunjukan_langsung,real_bap_aanwijzing_penunjukan_langsung', 22),
      (20, 'no_undangan_pengambilan_rks_pra,real_undangan_pengambilan_rks_pra', 23),
      (21, 'no_bap_aanwijzing_lelang_pra,real_bap_aanwijzing_lelang_pra', 24),
      (22, 'no_surat_penunjukan_pasca,real_surat_penunjukan_pasca', 25),
      (23, 'no_surat_penunjukan_langsung,real_surat_penunjukan_langsung', 27),
      (24, 'no_surat_penunjukan_pra,real_surat_penunjukan_pra', 26),
      (25, 'no_kontrak_pra,real_kontrak_pra', 30),
      (26, 'no_kontrak_pasca,real_kontrak_pasca', 30),
      (27, 'no_kontrak_pengadaan_langsung,real_kontrak_pengadaan_langsung', 30),
      (28, 'no_kontrak_penunjukan_langsung,real_kontrak_penunjukan_langsung', 30),
      (29, 'real_penyelesaian_pekerjaan', 29),
      (31, 'no_ba,real_ba,pemeriksaan_sesuai', 31),
      (32, 'ba_next', 32),
      (33, 'tagihan_return', 32),
      (34, 'real_tagihan_supplier_diterima', 33),
      (35, 'tagihan_verified', 35),
      (36, 'real_tagihan_diterima,real_bayar', 36),
      (37, 'real_tor_terbit', 2),
      (38, 'no_pr,real_terbit_pr,no_hps,nilai_hps', 5),
      (40, 'no_pr,real_terbit_pr', 5),
      (41, 'informasi_levering', 37),
      (42, 'pemeriksaan_tidak_sesuai', 37)
    ");
  }

  public function down () {
    $this->db->query("ALTER TABLE `document` DROP `real_tagihan_supplier_diterima`");
    $this->db->query("ALTER TABLE `document` DROP `ba_next`");
    $this->db->query("ALTER TABLE `document` DROP `perlu_hps`");
    $this->db->query("ALTER TABLE `document` ADD `jenis_pengadaan` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `document` DROP `lelang_terbatas`");
    $this->db->query("ALTER TABLE `document` DROP `lelang_terbuka`");
    $this->db->query("ALTER TABLE `document` DROP `pengadaan_langsung`");
    $this->db->query("ALTER TABLE `document` DROP `penunjukan_langsung`");
    $this->db->query("ALTER TABLE `document` DROP `lelang_prakualifikasi`");
    $this->db->query("ALTER TABLE `document` DROP `lelang_pascakualifikasi`");
    $this->db->query("ALTER TABLE `document` DROP `copy_kontrak`");
    $this->db->query("ALTER TABLE `document` DROP `pemeriksaan_sesuai`");
    $this->db->query("ALTER TABLE `document` DROP `pemeriksaan_tidak_sesuai`");
    $this->db->query("ALTER TABLE `document` DROP `tagihan_return`");
    $this->db->query("ALTER TABLE `document` DROP `tagihan_verified`");
  }
}