<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_formula extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document` ADD `plan_pprks` DATE NOT NULL");
  }

  public function down () {
    $this->db->query("ALTER TABLE `document` DROP `plan_pprks`");
  }

}
