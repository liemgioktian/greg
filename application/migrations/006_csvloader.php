<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_csvloader extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document` ADD `plan_tor_terbit` DATE NOT NULL AFTER `plan_pembayaran`");
    $this->db->query("ALTER TABLE `document` ADD `plan_terbit_pr` DATE NOT NULL AFTER `no_pr`");
    $this->db->query("ALTER TABLE `document` ADD `plan_po` DATE NOT NULL AFTER `real_pprks`");
  }

  public function down () {
    $this->db->query("ALTER TABLE `document` DROP `plan_tor_terbit`");
    $this->db->query("ALTER TABLE `document` DROP `plan_terbit_pr`");
    $this->db->query("ALTER TABLE `document` DROP `plan_po`");
  }
}