<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_statusnext extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `status` ADD `next` INT NOT NULL, ADD `duration` INT NOT NULL");
    $this->db->query("ALTER TABLE `document` ADD `last_status_update` DATE NOT NULL");
    $this->db->query("TRUNCATE `status`");
    $this->db->query("TRUNCATE `field_status`");
    $this->db->query("TRUNCATE `field_appearance`");
    $this->db->query("
    INSERT INTO `status` (`id`, `name`, `is_initial`, `is_final`, `next`, `duration`) VALUES
    (1, 'WAITING TOR', 1, 0, 2, 10),
    (2, 'TOR DITERIMA', 0, 0, 5, 8),
    (3, 'WAITING HPS', 0, 0, 4, 5),
    (4, 'INPUT NOMOR PR', 0, 0, 5, 3),
    (5, 'PR', 0, 0, 0, 0),
    (6, 'LELANG TERBATAS', 0, 0, 14, 5),
    (7, 'LELANG TERBUKA', 0, 0, 14, 0),
    (12, 'PP', 0, 0, 15, 17),
    (13, 'DOKUMEN PRAKUALIFIKASI', 0, 0, 17, 5),
    (14, 'RKS LELANG', 0, 0, 18, 5),
    (15, 'KONTRAK PENGADAAN LANGSUNG', 0, 0, 37, 1),
    (16, 'RKS PENUNJUKAN LANGSUNG', 0, 0, 20, 1),
    (17, 'SURAT PEMBERITAHUAN HASIL PRAKUALIFIKASI', 0, 0, 14, 1),
    (18, 'AANWIJZING LELANG PASCAKUALIFIKASI', 0, 0, 21, 24),
    (19, 'AANWIJZING PENUNJUKAN LANGSUNG', 0, 0, 22, 5),
    (20, 'UNDANGAN PENGAMBILAN RKS', 0, 0, 19, 5),
    (21, 'PENUNJUKAN LELANG PASCAKUALIFIKASI', 0, 0, 25, 1),
    (22, 'PENUNJUKAN (PENUNJUKAN LANGSUNG)', 0, 0, 27, 1),
    (23, 'AANWIJZING LELANG PRAKUALIFIKASI', 0, 0, 0, 0),
    (24, 'PENUNJUKAN LELANG PRAKUALIFIKASI', 0, 0, 0, 0),
    (25, 'PENERBITAN KONTRAK LELANG PASCAKUALIFIKASI', 0, 0, 37, 14),
    (26, 'PENERBITAN KONTRAK LELANG PRAKUALIFIKASI', 0, 0, 0, 0),
    (27, 'PENERBITAN KONTRAK PENUNJUKAN LANGSUNG', 0, 0, 37, 3),
    (29, 'BARANG DATANG / PEKERJAAN SELESAI', 0, 0, 0, 1),
    (30, 'INFORMASI PENGADAAN', 0, 0, 0, 0),
    (31, 'TERBIT BA', 0, 0, 0, 14),
    (32, 'MENUNGGU TAGIHAN SUPPLIER', 0, 0, 0, 0),
    (33, 'VERIFIKASI TAGIHAN SUPPLIER', 0, 0, 35, 3),
    (35, 'VERIFIKASI DAN PEMBAYARAN', 0, 0, 36, 14),
    (36, 'SELESAI', 0, 1, 0, 0),
    (37, 'LEVERING', 0, 0, 0, 0)
    ");
    $this->db->query("
    INSERT INTO `field_status` (`id`, `field`, `status`) VALUES
    (3, 'perlu_hps', 3),
    (4, 'tidak_perlu_hps', 4),
    (7, 'lelang_terbatas', 6),
    (8, 'lelang_terbuka', 7),
    (9, 'pengadaan_langsung', 12),
    (10, 'penunjukan_langsung', 16),
    (11, 'lelang_prakualifikasi', 13),
    (12, 'lelang_pascakualifikasi', 14),
    (13, 'no_pp_pengadaan_langsung,real_pp_pengadaan_langsung', 15),
    (14, 'no_dok_pra,real_dok_pra', 17),
    (15, 'no_rks_lelang_pasca,real_rks_lelang_pasca', 18),
    (16, 'no_rks_penunjukan_langsung,real_rks_penunjukan_langsung', 19),
    (17, 'no_surat_pemberitahuan_hasil_prakualifikasi,real_surat_pemberitahuan_hasil_prakualifikasi', 20),
    (18, 'no_bap_aanwijzing_lelang_pasca,real_bap_aanwijzing_lelang_pasca', 21),
    (19, 'no_bap_aanwijzing_penunjukan_langsung,real_bap_aanwijzing_penunjukan_langsung', 22),
    (20, 'no_undangan_pengambilan_rks_pra,real_undangan_pengambilan_rks_pra', 23),
    (21, 'no_bap_aanwijzing_lelang_pra,real_bap_aanwijzing_lelang_pra', 24),
    (22, 'no_surat_penunjukan_pasca,real_surat_penunjukan_pasca', 25),
    (23, 'no_surat_penunjukan_langsung,real_surat_penunjukan_langsung', 27),
    (24, 'no_surat_penunjukan_pra,real_surat_penunjukan_pra', 26),
    (25, 'no_kontrak_pra,real_kontrak_pra', 30),
    (26, 'no_kontrak_pasca,real_kontrak_pasca', 30),
    (27, 'no_kontrak_pengadaan_langsung,real_kontrak_pengadaan_langsung', 30),
    (28, 'no_kontrak_penunjukan_langsung,real_kontrak_penunjukan_langsung', 30),
    (29, 'real_penyelesaian_pekerjaan', 29),
    (31, 'no_ba,real_ba,pemeriksaan_sesuai', 31),
    (32, 'ba_next', 32),
    (33, 'tagihan_return', 32),
    (34, 'real_tagihan_supplier_diterima', 33),
    (35, 'tagihan_verified', 35),
    (36, 'real_tagihan_diterima,real_bayar', 36),
    (37, 'real_tor_terbit', 2),
    (38, 'no_pr,real_terbit_pr,no_hps,nilai_hps', 5),
    (40, 'no_pr,real_terbit_pr', 5),
    (41, 'informasi_levering', 37),
    (42, 'pemeriksaan_tidak_sesuai', 37)
    ");
    $this->db->query("
    INSERT INTO `field_appearance` (`id`, `field`, `status`) VALUES
    (3, 'perlu_hps,tidak_perlu_hps', 2),
    (6, 'lelang_terbatas,lelang_terbuka,pengadaan_langsung,penunjukan_langsung', 5),
    (7, 'lelang_prakualifikasi,lelang_pascakualifikasi', 6),
    (8, 'lelang_prakualifikasi,lelang_pascakualifikasi', 7),
    (9, 'no_dok_pra,real_dok_pra', 13),
    (10, 'no_pp_pengadaan_langsung,real_pp_pengadaan_langsung', 12),
    (11, 'no_rks_lelang_pasca,real_rks_lelang_pasca', 14),
    (12, 'no_rks_penunjukan_langsung,real_rks_penunjukan_langsung', 16),
    (13, 'no_kontrak_pengadaan_langsung,real_kontrak_pengadaan_langsung', 15),
    (14, 'no_surat_pemberitahuan_hasil_prakualifikasi,real_surat_pemberitahuan_hasil_prakualifikasi', 17),
    (15, 'no_bap_aanwijzing_lelang_pasca,real_bap_aanwijzing_lelang_pasca', 18),
    (16, 'no_bap_aanwijzing_penunjukan_langsung,real_bap_aanwijzing_penunjukan_langsung', 19),
    (17, 'no_undangan_pengambilan_rks_pra,real_undangan_pengambilan_rks_pra', 20),
    (18, 'no_surat_penunjukan_pasca,real_surat_penunjukan_pasca', 21),
    (19, 'no_surat_penunjukan_langsung,real_surat_penunjukan_langsung', 22),
    (20, 'no_bap_aanwijzing_lelang_pra,real_bap_aanwijzing_lelang_pra', 23),
    (21, 'no_kontrak_pasca,real_kontrak_pasca', 25),
    (22, 'no_kontrak_penunjukan_langsung,real_kontrak_penunjukan_langsung', 27),
    (23, 'no_surat_penunjukan_pra,real_surat_penunjukan_pra', 24),
    (24, 'no_kontrak_pra,real_kontrak_pra', 26),
    (29, 'ba_next', 31),
    (30, 'real_tagihan_supplier_diterima', 32),
    (32, 'tagihan_return,tagihan_verified', 33),
    (33, 'real_tagihan_diterima,real_bayar', 35),
    (40, 'rab', 2),
    (41, 'real_tor_terbit', 1),
    (42, 'no_pr,real_terbit_pr,no_hps,nilai_hps', 3),
    (43, 'no_pr,real_terbit_pr', 4),
    (44, 'informasi_levering', 30),
    (45, 'real_penyelesaian_pekerjaan', 37),
    (46, 'no_ba,real_ba,pemeriksaan_sesuai,pemeriksaan_tidak_sesuai,pemeriksaan_note', 29),
    (47, 'rab,plan_tor_terbit', 0)
    ");
  }

  public function down () {
    $this->db->query("ALTER TABLE `status` DROP `next`, DROP `duration`");
    $this->db->query("ALTER TABLE `document` DROP `last_status_update`");
  }

}