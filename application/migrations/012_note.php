<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_note extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document` ADD `pemeriksaan_note` TEXT NOT NULL");
  }

  public function down () {
    $this->db->query("ALTER TABLE `document` DROP `pemeriksaan_note`");
  }

}