<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_statusstartingfield extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `status` ADD `starting_field` VARCHAR(255) NOT NULL");
  }

  public function down () {
    $this->db->query("ALTER TABLE `status` DROP `starting_field`");
  }

}