<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_initialconfig extends CI_Migration {

  public function up()
  {
    $this->db->query("TRUNCATE `notification`");
    $this->db->query("
      INSERT INTO `notification` (`id`, `label`, `config`, `message`) VALUES
      (2, 'RENCANA TANGGAL TERBIT PR DENGAN HPS', 8, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PR Terlambat'),
      (3, 'RENCANA TANGGAL TERBIT PR TANPA HPS', 3, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PR Terlambat'),
      (4, 'RENCANA PP/RKS PENGADAAN LANGSUNG', 6, ''),
      (5, 'RENCANA PP/RKS LELANG PRAKUALIFIKASI', 32, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PP/RKS Lelang Prakualifikasi Terlambat'),
      (6, 'RENCANA PP/RKS LELANG PASCAKUALIFIKASI', 5, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PP/RKS Lelang Pascakualifikasi Terlambat'),
      (8, 'RENCANA PO PENGADAAN LANGSUNG', 6, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PO Pengadaan Langsung Terlambat'),
      (9, 'RENCANA PO LELANG PRAKUALIFIKASI', 18, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PO Lelang Prakualifikasi Terlambat'),
      (10, 'RENCANA PO LELANG PASCAKUALIFIKASI', 30, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : PO Lelang Pascakualisikasi Terlambat'),
      (11, 'TARGET PENERIMAAN', 16, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : Penerimaan Terlambat'),
      (12, 'TARGET PENAGIHAN', 2, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : Penagihan Terlambat'),
      (13, 'TARGET TAGIHAN DITERIMA', 3, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : Tagihan Diterima Terlambat'),
      (14, 'TARGET PEMBAYARAN', 15, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : Pembayaran Terlambat'),
      (15, 'RENCANA TANGGAL TOR TERBIT', 0, 'Nama Proyek                         :{{Nama/Judul Pekerjaan}}\r\nNomor TOR                           :{{Nomor TOR}}\r\nRAB Pekerjaan                      :Rp.{{RAB Pekerjaan}}\r\nTarget Tanggal Penerimaan   :{{Target Penerimaan}}\r\nTarget Pembayaran                :{{Target Pembayaran}}\r\nStatus                                     : TOR Terlambat')
    ");
    $this->db->query("TRUNCATE `role`");
    $this->db->query("
      INSERT INTO `role` (`id`, `name`) VALUES
      (1, 'SUPERADMIN'),
      (2, 'INVENTORY'),
      (3, 'PENGADAAN'),
      (4, 'GUDANG'),
      (5, 'KEUANGAN')
    ");
    $this->db->query("TRUNCATE `role_status`");
    $this->db->query("
      INSERT INTO `role_status` (`id`, `status`, `role`) VALUES
      (1, 1, 2),
      (2, 2, 2),
      (3, 3, 2),
      (4, 4, 2),
      (5, 5, 3),
      (6, 6, 3),
      (7, 7, 3),
      (8, 12, 3),
      (9, 13, 3),
      (10, 14, 3),
      (11, 15, 3),
      (12, 16, 3),
      (13, 17, 3),
      (14, 18, 3),
      (16, 19, 3),
      (17, 20, 3),
      (18, 21, 3),
      (19, 22, 3),
      (20, 23, 3),
      (21, 23, 3),
      (22, 24, 3),
      (23, 25, 3),
      (24, 26, 3),
      (25, 27, 3),
      (26, 29, 4),
      (27, 30, 4),
      (28, 31, 4),
      (29, 32, 3),
      (30, 33, 3),
      (31, 35, 5),
      (32, 36, 5),
      (33, 37, 4)
    ");
    $this->db->query("TRUNCATE `status`");
    $this->db->query("
      INSERT INTO `status` (`id`, `name`, `is_initial`, `is_final`, `next`, `duration`, `starting_field`) VALUES
      (1, 'WAITING TOR', 1, 1, 2, 0, 'plan_tor_terbit'),
      (2, 'TOR DITERIMA', 0, 0, 5, 8, 'real_tor_terbit'),
      (3, 'WAITING HPS', 0, 0, 4, 5, ''),
      (4, 'INPUT NOMOR PR', 0, 0, 5, 3, ''),
      (5, 'PR', 0, 0, 0, 0, ''),
      (6, 'LELANG TERBATAS', 0, 0, 14, 5, ''),
      (7, 'LELANG TERBUKA', 0, 0, 14, 0, ''),
      (12, 'PP', 0, 0, 15, 17, ''),
      (13, 'DOKUMEN PRAKUALIFIKASI', 0, 0, 17, 5, ''),
      (14, 'RKS LELANG', 0, 0, 18, 5, ''),
      (15, 'KONTRAK PENGADAAN LANGSUNG', 0, 0, 37, 1, ''),
      (16, 'RKS PENUNJUKAN LANGSUNG', 0, 0, 20, 1, ''),
      (17, 'SURAT PEMBERITAHUAN HASIL PRAKUALIFIKASI', 0, 0, 14, 1, ''),
      (18, 'AANWIJZING LELANG PASCAKUALIFIKASI', 0, 0, 21, 24, ''),
      (19, 'AANWIJZING PENUNJUKAN LANGSUNG', 0, 0, 22, 5, ''),
      (20, 'UNDANGAN PENGAMBILAN RKS', 0, 0, 19, 5, ''),
      (21, 'PENUNJUKAN LELANG PASCAKUALIFIKASI', 0, 0, 25, 1, ''),
      (22, 'PENUNJUKAN (PENUNJUKAN LANGSUNG)', 0, 0, 27, 1, ''),
      (23, 'AANWIJZING LELANG PRAKUALIFIKASI', 0, 0, 0, 0, ''),
      (24, 'PENUNJUKAN LELANG PRAKUALIFIKASI', 0, 0, 0, 0, ''),
      (25, 'PENERBITAN KONTRAK LELANG PASCAKUALIFIKASI', 0, 0, 37, 14, ''),
      (26, 'PENERBITAN KONTRAK LELANG PRAKUALIFIKASI', 0, 0, 0, 0, ''),
      (27, 'PENERBITAN KONTRAK PENUNJUKAN LANGSUNG', 0, 0, 37, 3, ''),
      (29, 'BARANG DATANG / PEKERJAAN SELESAI', 0, 0, 0, 1, ''),
      (30, 'INFORMASI PENGADAAN', 0, 0, 0, 0, ''),
      (31, 'TERBIT BA', 0, 0, 0, 14, ''),
      (32, 'MENUNGGU TAGIHAN SUPPLIER', 0, 0, 0, 0, ''),
      (33, 'VERIFIKASI TAGIHAN SUPPLIER', 0, 0, 35, 3, ''),
      (35, 'VERIFIKASI DAN PEMBAYARAN', 0, 0, 36, 14, ''),
      (36, 'SELESAI', 0, 0, 0, 0, ''),
      (37, 'LEVERING', 0, 0, 0, 0, '')
    ");
    $this->db->query("TRUNCATE `admin`");
    $this->db->query("
      INSERT INTO `admin` (`id`, `role`, `username`, `password`, `email`, `notified`) VALUES
      (1, 1, 'superadmin', '202cb962ac59075b964b07152d234b70', '', 0),
      (2, 2, 'logininventory', '202cb962ac59075b964b07152d234b70', 'joggrex@gmail.com, mae_seronito@yahoo.com, agung.khurniawan@gmail.com, alan.laksono@gmail.com, fendhitopratamaputra@gmail.com', 1),
      (3, 3, 'loginpengadaan', '202cb962ac59075b964b07152d234b70', 'fendhitopratamaputra@gmail.com', 1),
      (4, 4, 'logingudang', '202cb962ac59075b964b07152d234b70', 'alan.laksono@gmail.com', 1),
      (5, 5, 'loginkeuangan', '202cb962ac59075b964b07152d234b70', 'hery.upmk@gmail.com', 1)
    ");
  }

  public function down () {
    $this->db->query("TRUNCATE `notification`");
    $this->db->query("TRUNCATE `role`");
    $this->db->query("TRUNCATE `role_status`");
    $this->db->query("TRUNCATE `status`");
    $this->db->query("TRUNCATE `admin`");
  }

}