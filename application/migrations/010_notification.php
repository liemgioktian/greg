<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_notification extends CI_Migration {

  public function up()
  {
    $this->db->query("RENAME TABLE `globalconfig` TO `notification`");
    $this->db->query("ALTER TABLE `notification` ADD `message` TEXT NOT NULL");
    $this->db->query("ALTER TABLE `admin` ADD `email` VARCHAR(255) NOT NULL");
    $this->db->query("ALTER TABLE `admin` ADD `notified` TINYINT NOT NULL");
  }

  public function down () {
    $this->db->query("ALTER TABLE `notification` DROP `message`");
    $this->db->query("RENAME TABLE `notification` TO `globalconfig`");
    $this->db->query("ALTER TABLE `admin` DROP `email`");
    $this->db->query("ALTER TABLE `admin` DROP `notified`");
  }

}
 