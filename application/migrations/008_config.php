<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_config extends CI_Migration {

  public function up()
  {
    $this->db->query("
      CREATE TABLE IF NOT EXISTS `globalconfig` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `label` varchar(255) NOT NULL,
        `config` int(11) NOT NULL,
        PRIMARY KEY `id` (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      INSERT INTO `globalconfig` (`id`, `label`, `config`) VALUES
      (2, 'RENCANA TANGGAL TERBIT PR DENGAN HPS', 8),
      (3, 'RENCANA TANGGAL TERBIT PR TANPA HPS', 3),
      (4, 'RENCANA PP/RKS PENGADAAN LANGSUNG', 6),
      (5, 'RENCANA PP/RKS LELANG PRAKUALIFIKASI', 32),
      (6, 'RENCANA PP/RKS LELANG PASCAKUALIFIKASI', 5),
      (8, 'RENCANA PO PENGADAAN LANGSUNG', 6),
      (9, 'RENCANA PO LELANG PRAKUALIFIKASI', 18),
      (10, 'RENCANA PO LELANG PASCAKUALIFIKASI', 30),
      (11, 'TARGET PENERIMAAN', 14),
      (12, 'TARGET PENAGIHAN', 2),
      (13, 'TARGET TAGIHAN DITERIMA', 3),
      (14, 'TARGET PEMBAYARAN', 15)
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE IF EXISTS globalconfig");
  }

}