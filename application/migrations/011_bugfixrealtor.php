<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_bugfixrealtor extends CI_Migration {

  public function up()
  {
    $this->db->query("ALTER TABLE `document` DROP `real_tor_diterima`");
  }

  public function down () {
    $this->db->query("ALTER TABLE `document` ADD `real_tor_diterima` DATE NOT NULL");
  }

}
 
